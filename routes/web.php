<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@main')->name('page.main');
Route::get('/about', 'PageController@about')->name('page.about');
Route::get('/contacts', 'PageController@contacts')->name('page.contacts');
Route::get('/policy', 'PageController@policy')->name('page.policy');
Route::get('/cookie', 'PageController@cookie')->name('page.cookie');
Route::resource('service', 'ServiceController')->only(['show']);
Route::get('/services', 'ServiceController@index')->name('service.index');
Route::resource('project', 'ProjectController')->only(['show']);
Route::get('/projects', 'ProjectController@index')->name('project.index');
Route::resource('product', 'ProductController')->only(['show']);
Route::get('/products/{categoria?}/{attributes?}', 'ProductController@index')->name('product.index');
Route::resource('problem', 'ProblemController')->only('store');
Route::post('/enter', 'UserController@login')->name('user.login');
Route::get('/login', 'UserController@loginPage')->name('user.login_page');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/logout', 'UserController@logout')->name('user.logout');
      
    Route::resource('attribute', 'AttributeController')->except(['show']);
    Route::resource('attribute_type', 'AttributeTypeController')->except(['show']);
    Route::resource('problem', 'ProblemController')->only(['destroy', 'index', 'show']);
    Route::resource('product', 'ProductController')->except(['index', 'show']);
    Route::get('/products', 'ProductController@listing')->name('product.listing');
    Route::resource('product_category', 'ProductCategoryController')->except(['show']);
    Route::resource('common_seo', 'CommonSeoController')->only(['edit', 'update']);
	Route::resource('worker', 'WorkerController')->except(['show']);
    Route::resource('license', 'LicenseController')->except(['show']);
    Route::resource('banner', 'BannerController')->except(['show']);
    Route::resource('setting', 'SettingController')->only(['update', 'edit']);
    Route::resource('user', 'UserController')->except('show');
    Route::resource('project', 'ProjectController')->except(['index', 'show', 'update']);
    Route::post('/project_store/{id}', 'ProjectController@update');
    Route::get('/projects', 'ProjectController@listing')->name('project.listing');
    Route::resource('service', 'ServiceController')->except(['index', 'show']);
    Route::get('/services', 'ServiceController@listing')->name('service.listing');
    Route::get('/common_image/{id}', 'CommonImageController@destroy')->name('common_image.destroy');
});