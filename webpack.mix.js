const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//admin assets
mix.scripts(
    [
        'resources/assets/js/admin/vendors/jquery.js',
        'resources/assets/js/admin/bootstrap.js',
        'resources/assets/js/admin/bootstrap-select.js',
        'resources/assets/js/admin/jquery.slimscroll.js',
        'resources/assets/js/admin/waves.js',
        'resources/assets/js/admin/autosize.js',
        'resources/assets/js/admin/moment.js',
        'resources/assets/js/admin/modals.js',
        'resources/assets/js/admin/bootstrap-material-datetimepicker.js',
        'resources/assets/js/admin/admin.js',
        'resources/assets/js/admin/basic-form-elements.js',
        'resources/assets/js/admin/demo.js',
        'resources/assets/js/admin/image_upload.js',
        'resources/assets/js/admin/prod_cat_attributes.js'
    ], 'public/js/admin.js')
   .sass('resources/assets/sass/admin/app.scss', 'public/css/admin.css');


//site frontend
mix.scripts(
    [
        'resources/assets/js/admin/vendors/jquery.js',
        'resources/assets/js/site/jquery-ui/jquery-ui.js',
        'resources/assets/js/site/ui-kit/uikit.js',
        'resources/assets/js/site/ui-kit/components/parallax.js',
        'resources/assets/js/site/vendors/components/tab.js',
        'resources/assets/js/site/vendors/components/accordion.js',
        'resources/assets/js/site/vendors/components/switcher.js',
        'resources/assets/js/site/ui-kit/core/modal.js',
        'resources/assets/js/site/ui-kit/core/scrollspy.js',
        'resources/assets/js/site/ui-kit/components/lightbox.js',
        'resources/assets/js/site/parallax.js',
        'resources/assets/js/site/initialization.js',
        'resources/assets/js/site/owl.carousel.js',
        'resources/assets/js/site/common.js'
    ], 'public/js/site.js')
    .sass('resources/assets/sass/site/app.scss', 'public/css/site.css');

