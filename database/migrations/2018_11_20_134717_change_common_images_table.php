<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCommonImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('common_images', function (Blueprint $table) {
            $table->string('img');
            $table->integer('entity_id')->unsigned();
            $table->string('entity_type');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('common_images', function (Blueprint $table) {
            $table->dropColumn(['img', 'entity_id', 'entity_type']);
        });
    }
}
