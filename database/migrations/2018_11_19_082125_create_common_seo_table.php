<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonSeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('common_seo', function (Blueprint $table) {
            $table->increments('id');

            $table->string('about_title');
            $table->string('about_description');
            $table->string('about_keywords');
            $table->string('about_og_image');
            $table->string('about_vk_image');

            $table->string('contacts_title');
            $table->string('contacts_description');
            $table->string('contacts_keywords');
            $table->string('contacts_og_image');
            $table->string('contacts_vk_image');

            $table->string('catalog_title');
            $table->string('catalog_description');
            $table->string('catalog_keywords');
            $table->string('catalog_og_image');
            $table->string('catalog_vk_image');

            $table->string('main_title');
            $table->string('main_description');
            $table->string('main_keywords');
            $table->string('main_og_image');
            $table->string('main_vk_image'); 

            $table->string('projects_title');
            $table->string('projects_description');
            $table->string('projects_keywords');
            $table->string('projects_og_image');
            $table->string('projects_vk_image'); 

            $table->string('services_title');
            $table->string('services_description');
            $table->string('services_keywords');
            $table->string('services_og_image');
            $table->string('services_vk_image'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('common_seo');
    }
}
