<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\CommonSeo;
use App\Models\Setting;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'product' => 'App\Models\Product',
            'project' => 'App\Models\Project',
            'service' => 'App\Models\Service',
        ]);

        $commonSeo = CommonSeo::first();
        $setting = Setting::first();

        view()->share(
            [
                'commonSeo' => $commonSeo,
                'setting' => $setting,
            ]
        );
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
