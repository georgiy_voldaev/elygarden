<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'attributes';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'type_id',
        'type_cat_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function type()
    {
        return $this->belongsTo('App\Models\AttributeType', 'type_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'type_cat_id', 'id');
    }

    /**
     * Возвращает условия для Eloquent построителя запросов
     * Условие - коревой уровень категорий.
     *
     * @param $query
     * @param $categoryId
     * @return mixed
     */
    public function scopeCategoryAttributes($query, $categoryId)
    {
        return $query->where('type_cat_id', '=', $categoryId);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->belongsToMany(
            'App\Models\Product', 
            'product_attribute', 
            'product_id', 
            'attribute_id'
        );
    }
}
