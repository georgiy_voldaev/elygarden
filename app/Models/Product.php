<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Product extends BaseFile
{
    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'old_cost',
        'cost',
        'slug',
        'description',
        'short_description',
        'status',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'seo_og_image',
        'seo_vk_image',
        'category_id',
    ];
    
    /**
     * @var array
     */
    public $variants = [
        'og_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 630,
            ]
        ],
        'vk_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 536,
            ]
        ],
    ];
    
    /**
     * @param $value
     */
    public function setSeoVkImageAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['seo_vk_image'])) {
                $this->attributes['seo_vk_image'] = $this->saveFiles($this->variants['vk_image'], $value);
                $this->attributes['seo_og_image'] = $this->saveFiles($this->variants['og_image'], $value);
            } else {
                $this->attributes['seo_vk_image'] = $this->saveFiles($this->variants['vk_image'], $value, $this->attributes['seo_vk_image']);
                $this->attributes['seo_og_image'] = $this->saveFiles($this->variants['og_image'], $value, $this->attributes['seo_og_image']);
            }
        }
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->belongsToMany(
            'App\Models\Attribute', 
            'product_attribute',
            'product_id', 
            'attribute_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commonImages()
    {
        return $this->morphMany('App\Models\CommonImage', 'entity', null, 'entity_id', 'id');
    }

    /**
     * @param $value
     */
    public function setNameAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['name'])) {
                $this->attributes['name'] = $value;
                $this->attributes['slug'] = str_slug($value).'_'.Carbon::now();
            } else {
                $this->attributes['name'] = $value;
                $this->attributes['slug'] = str_slug($value).'_'.$this->attributes['created_at'];
            }
        }
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($model)
        {
            $model->deleteAll($model->variants['og_image'], $model->seo_og_image);
            $model->deleteAll($model->variants['vk_image'], $model->seo_vk_image);
        });
    }
}
