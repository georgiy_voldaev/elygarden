<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Service extends BaseFile
{    
    /**
     * @var string
     */
    protected $table = 'services';

    /**
     * @var array
     */
    protected $fillable = [
        'img',
        'name',
        'description',
        'short_description',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'og_vk_image',
        'og_all_image',
        'slug',
    ];

    /**
     * @var array
     */
    public $variants = [
        'img' => [
            0 => [
                'name' => 'large',
                'width' => 1270,
                'height' => 700,
            ],
            1 => [
                'name' => 'middle',
                'width' => 300,
                'height' => 250,
            ],
            2 => [
                'name' => 'min',
                'width' => 650,
                'height' => 520,
            ],
            3 => [
                'name' => 'cover',
                'width' => 1920,
                'height' => 400
            ]
        ],
        'og_all_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 630,
            ]
        ],
        'og_vk_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 536,
            ]
        ],
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects ()
    {
        return $this->belongsToMany(
            'App\Models\Project', 
            'project_service',
            'service_id', 
            'project_id'
        );
    }

    public function commonImages()
    {
        return $this->morphMany('App\Models\CommonImage', 'entity', null, 'entity_id', 'id');
    }

    /**
     * @param $value
     */
    public function setImgAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['img'])) {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value);
            } else {
            }
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value, $this->attributes['img']);
        }
    }

     /**
     * @param $value
     */
    public function setOgVkImageAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['og_vk_image'])) {
                $this->attributes['og_vk_image'] = $this->saveFiles($this->variants['og_vk_image'], $value);
                $this->attributes['og_all_image'] = $this->saveFiles($this->variants['og_all_image'], $value);
            } else {
                $this->attributes['og_vk_image'] = $this->saveFiles($this->variants['og_vk_image'], $value, $this->attributes['og_vk_image']);
                $this->attributes['og_all_image'] = $this->saveFiles($this->variants['og_all_image'], $value, $this->attributes['og_all_image']);
            }
        }
    }

    /**
     * @param $value
     */
    public function setNameAttribute ($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value).'_'.Carbon::now();
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($model)
        {
            $model->deleteAll($model->variants['img'], $model->img);
            $model->deleteAll($model->variants['og_vk_image'], $model->og_vk_image);
            $model->deleteAll($model->variants['og_all_image'], $model->og_all_image);
        });
    }
}
