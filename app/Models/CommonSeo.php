<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonSeo extends BaseFile
{
    public $timestamps = false;
    
    /**
     * @var string
     */
    protected $table = 'common_seo';

    /**
     * @var array
     */
    protected $fillable = [
        'about_title',
        'about_description',
        'about_keywords',
        'about_og_image',
        'about_vk_image',

        'contacts_title',
        'contacts_description',
        'contacts_keywords',
        'contacts_og_image',
        'contacts_vk_image',
        
        'catalog_title',
        'catalog_description',
        'catalog_keywords',
        'catalog_og_image',
        'catalog_vk_image',

        'main_title',
        'main_description',
        'main_keywords',
        'main_og_image',
        'main_vk_image',

        'services_title',
        'services_description',
        'services_keywords',
        'services_og_image',
        'services_vk_image',

        'projects_title',
        'projects_description',
        'projects_keywords',
        'projects_og_image',
        'projects_vk_image',
    ];

    /**
     * @var array
     */
    public $variants = [
        'about_og_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 630,
            ]
        ],
        'about_vk_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 536,
            ]
        ],
        'contacts_og_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 630,
            ]
        ],
        'contacts_vk_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 536,
            ]
        ],
        'catalog_og_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 630,
            ]
        ],
        'catalog_vk_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 536,
            ]
        ],
        'main_og_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 630,
            ]
        ],
        'main_vk_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 536,
            ]
        ],
        'services_og_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 630,
            ]
        ],
        'services_vk_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 536,
            ]
        ],
        'projects_og_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 630,
            ]
        ],
        'projects_vk_image' => [
            0 => [
                'name' => 'default',
                'width' => 1200,
                'height' => 536,
            ]
        ],
    ];

    /**
     * @param $value
     */
    public function setAboutVkImageAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['about_vk_image'])) {
                $this->attributes['about_vk_image'] = $this->saveFiles($this->variants['about_vk_image'], $value);
                $this->attributes['about_og_image'] = $this->saveFiles($this->variants['about_og_image'], $value);
            } else {
                $this->attributes['about_vk_image'] = $this->saveFiles($this->variants['about_vk_image'], $value, $this->attributes['about_vk_image']);
                $this->attributes['about_og_image'] = $this->saveFiles($this->variants['about_og_image'], $value, $this->attributes['about_og_image']);
            }
        }
    }

    /**
     * @param $value
     */
    public function setContactsVkImageAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['contacts_vk_image'])) {
                $this->attributes['contacts_vk_image'] = $this->saveFiles($this->variants['contacts_vk_image'], $value);
                $this->attributes['contacts_og_image'] = $this->saveFiles($this->variants['contacts_og_image'], $value);
            } else {
                $this->attributes['contacts_vk_image'] = $this->saveFiles($this->variants['contacts_vk_image'], $value, $this->attributes['contacts_vk_image']);
                $this->attributes['contacts_og_image'] = $this->saveFiles($this->variants['contacts_og_image'], $value, $this->attributes['contacts_og_image']);
            }
        }
    }

    /**
     * @param $value
     */
    public function setCatalogVkImageAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['catalog_vk_image'])) {
                $this->attributes['catalog_vk_image'] = $this->saveFiles($this->variants['catalog_vk_image'], $value);
                $this->attributes['catalog_og_image'] = $this->saveFiles($this->variants['catalog_og_image'], $value);
            } else {
                $this->attributes['catalog_vk_image'] = $this->saveFiles($this->variants['catalog_vk_image'], $value, $this->attributes['catalog_vk_image']);
                $this->attributes['catalog_og_image'] = $this->saveFiles($this->variants['catalog_og_image'], $value, $this->attributes['catalog_og_image']);
            }
        }
    }

    /**
     * @param $value
     */
    public function setMainVkImageAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['main_vk_image'])) {
                $this->attributes['main_vk_image'] = $this->saveFiles($this->variants['main_vk_image'], $value);
                $this->attributes['main_og_image'] = $this->saveFiles($this->variants['main_og_image'], $value);
            } else {
                $this->attributes['main_vk_image'] = $this->saveFiles($this->variants['main_vk_image'], $value, $this->attributes['main_vk_image']);
                $this->attributes['main_og_image'] = $this->saveFiles($this->variants['main_og_image'], $value, $this->attributes['main_og_image']);
            }
        }
    }

    /**
     * @param $value
     */
    public function setServicesVkImageAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['services_vk_image'])) {
                $this->attributes['services_vk_image'] = $this->saveFiles($this->variants['services_vk_image'], $value);
                $this->attributes['services_og_image'] = $this->saveFiles($this->variants['services_og_image'], $value);
            } else {
                $this->attributes['services_vk_image'] = $this->saveFiles($this->variants['services_vk_image'], $value, $this->attributes['services_vk_image']);
                $this->attributes['services_og_image'] = $this->saveFiles($this->variants['services_og_image'], $value, $this->attributes['services_og_image']);
            }
        }
    }

    /**
     * @param $value
     */
    public function setProjectsVkImageAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['projects_vk_image'])) {
                $this->attributes['projects_vk_image'] = $this->saveFiles($this->variants['projects_vk_image'], $value);
                $this->attributes['projects_og_image'] = $this->saveFiles($this->variants['projects_og_image'], $value);
            } else {
                $this->attributes['projects_vk_image'] = $this->saveFiles($this->variants['projects_vk_image'], $value, $this->attributes['projects_vk_image']);
                $this->attributes['projects_og_image'] = $this->saveFiles($this->variants['projects_og_image'], $value, $this->attributes['projects_og_image']);
            }
        }
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($model)
        {
            $model->deleteAll($model->variants['about_og_image'], $model->about_og_image);
            $model->deleteAll($model->variants['about_vk_image'], $model->about_vk_image);
            $model->deleteAll($model->variants['contacts_og_image'], $model->contacts_og_image);
            $model->deleteAll($model->variants['contacts_vk_image'], $model->contacts_vk_image);
            $model->deleteAll($model->variants['catalog_og_image'], $model->catalog_og_image);
            $model->deleteAll($model->variants['catalog_vk_image'], $model->catalog_vk_image);
            $model->deleteAll($model->variants['main_og_image'], $model->main_og_image);
            $model->deleteAll($model->variants['main_vk_image'], $model->main_vk_image);
            $model->deleteAll($model->variants['services_og_image'], $model->services_og_image);
            $model->deleteAll($model->variants['services_vk_image'], $model->services_vk_image);
            $model->deleteAll($model->variants['projects_og_image'], $model->projects_og_image);
            $model->deleteAll($model->variants['projects_vk_image'], $model->projects_vk_image);
        });
    }
}
