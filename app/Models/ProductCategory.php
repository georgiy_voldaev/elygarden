<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends BaseFile
{
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'product_categories';

    /**
     * @var array
     */
    protected $fillable = [
        'img',
        'name',
        'parent_id',
    ];

    protected $attributes = array(
        'parent_id' => 0
    );

    /**
     * @var array
     */
    public $variants = [
        'img' => [
            0 => [
                'name' => 'default',
                'width' => 500,
                'height' => 500,
            ],
            1 => [
                'name' => 'middle',
                'width' => 1100,
                'height' => 400,
            ],
            2 => [
                'name' => 'min',
                'width' => 500,
                'height' => 400,
            ]
        ],
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany('App\Models\Attribute', 'type_cat_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributeTypes()
    {
        return $this->belongsToMany('App\Models\AttributeType', 'attribute_type_category', 'category_id', 'attribute_type_id');
    }

    /**
     * Возвращает детей запрошенной категории.
     *
     * @return HasMany
     */
    public function subCategories()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    /**
     * Возвращает родителя запрошенной категории.
     *
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    /**
     * Возвращает условия для Eloquent построителя запросов
     * Условие - коревой уровень категорий.
     *
     * @param $query
     * @return mixed
     */
    public function scopeMain($query)
    {
        return $query->where('parent_id', 0);
    }

    /**
     * @param $value
     */
    public function setImgAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['img'])) {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value);
            } else {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value, $this->attributes['img']);
            }
        }
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($model)
        {
            $model->deleteAll($model->variants['img'], $model->img);
        });
    }
}
