<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends BaseFile
{
    public $timestamps = false;
    
   /**
     * @var string
     */
    protected $table = 'banners';

    /**
     * @var array
     */
    protected $fillable = [
        'img',
        'title',
    ];

    /**
     * @var array
     */
    public $variants = [
        'img' => [
            0 => [
                'name' => 'default',
                'width' => 1920,
                'height' => 650,
            ],
            1 => [
                'name' => 'middle',
                'width' => 1100,
                'height' => 400,
            ],
            2 => [
                'name' => 'min',
                'width' => 500,
                'height' => 400,
            ]
        ],
    ];

    /**
     * @param $value
     */
    public function setImgAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['img'])) {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value);
            } else {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value, $this->attributes['img']);
            }
        }
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($model)
        {
            $model->deleteAll($model->variants['img'], $model->img);
        });
    }
}
