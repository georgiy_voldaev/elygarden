<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class License extends BaseFile
{
    public $timestamps = false;
    
    /**
     * @var string
     */
    protected $table = 'licenses';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'img',
    ];

    /**
     * @var array
     */
    public $variants = [
        'img' => [
            0 => [
                'name' => 'default',
                'width' => 250,
                'height' => 250,
            ]
        ],
    ];

    /**
     * @param $value
     */
    public function setImgAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['img'])) {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value);
            } else {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value, $this->attributes['img']);
            }
        }
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($model)
        {
            $model->deleteAll($model->variants['img'], $model->img);
        });
    }
}
