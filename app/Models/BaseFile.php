<?php

namespace App\Models;

use App\Traits\ImageUpload;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseFile
 * @package App\Models
 */
class BaseFile extends Model
{
    use ImageUpload;

    /**
     * @param string $variant
     * @param string $field
     * @return string
     */
    public function img (string $variant, string $field) : string
    {
        return str_replace('default', $variant, $this->$field);
    }
}
