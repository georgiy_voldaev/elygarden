<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonImage extends BaseFile
{
     /**
     * @var string
     */
    protected $table = 'common_images';

    /**
     * @var array
     */
    protected $fillable = [
        'img',
        'entity_id',
        'entity_type',
    ];

    /**
     * @var array
     */
    public $variants = [
        'img' => [
            0 => [
                'name' => 'default',
                'width' => 1270,
                'height' => 700,
            ],
            1 => [
                'name' => 'dot',
                'width' => 80,
                'height' => 80,
            ],
            2 => [
                'name' => 'catalog_preview',
                'width' => 400,
                'height' => 400,
            ],
            3 => [
                'name' => 'slide',
                'width' => 650,
                'height' => 650,
            ]
        ],
    ];

    public function entity()
    {
        return $this->morphTo();
    }

    /**
     * @param $value
     */
    public function setImgAttribute ($value)
    {
        if (!empty($value)) {
            if (empty($this->attributes['img'])) {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value);
            } else {
                $this->attributes['img'] = $this->saveFiles($this->variants['img'], $value, $this->attributes['img']);
            }
        }
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function($model)
        {
            $model->deleteAll($model->variants['img'], $model->img);
        });
    }
}
