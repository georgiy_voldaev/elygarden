<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{
     /**
     * @var string
     */
    protected $table = 'problems';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'email',
        'message',
        'status',
    ];
}
