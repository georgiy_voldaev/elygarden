<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Http\Requests\SettingRequest;

class SettingController extends Controller
{
    /**
     * @var
     */
    private $setting;

    /**
     * @param Setting $setting
     */
    public function __construct (
        Setting $setting
    )
    {
        $this->setting = $setting;
    }   
  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = $this->setting::findOrFail($id);

        return view('admin.settings.edit', ['setting' => $setting]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(SettingRequest $request, $id)
    {
        $this->setting::findOrFail($id)->update($request->except('_token'));

        return redirect()->back()->with('message', 'Настройки успешно изменены!');
    }
}
