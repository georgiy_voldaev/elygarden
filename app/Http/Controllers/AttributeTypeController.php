<?php

namespace App\Http\Controllers;

use App\Models\AttributeType;
use App\Models\ProductCategory;
use App\Http\Requests\AttributeTypeRequest;
use DB;
class AttributeTypeController extends Controller
{
    /**
     * @var
     */
    private $attributeType;

    /**
     * @var
     */
    private $productCategory;

    /**
     * @param AttributeType $attributeType
     */
    public function __construct (
        AttributeType $attributeType,
        ProductCategory $productCategory
    )
    {
        $this->attributeType = $attributeType;
        $this->productCategory = $productCategory;
    }   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributeTypes = $this->attributeType::paginate(15);

        return view('admin.attribute_types.list', ['attributeTypes' => $attributeTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->productCategory::all();

        return view('admin.attribute_types.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeTypeRequest $request)
    {
        $attributeType = $this->attributeType::create($request->except('_token'));

        $categories = $request->get('category_id');
        $attributeType->categories()->sync($categories);                            

        return redirect()->route('attribute_type.index')->with('message', 'Новый тип атрибута успешно добавлен!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\attributeType  $attributeType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = $this->productCategory::all();
        $attributeType = $this->attributeType::findOrFail($id);

        return view('admin.attribute_types.edit', 
            [
                'attributeType' => $attributeType,
                'categories' => $categories,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\attributeType  $attributeType
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeTypeRequest $request, $id)
    {
        $attributeType = $this->attributeType::findOrFail($id);
        $attributeType->update($request->except('_token'));

        $categories = $request->get('category_id');
        $attributeType->categories()->sync($categories);

        return redirect()->route('attribute_type.index')->with('message', 'Тип атрибута успешно изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\attributeType  $attributeType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->attributeType::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Тип атрибута успешно удален!');
    }
}
