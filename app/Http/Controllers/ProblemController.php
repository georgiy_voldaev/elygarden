<?php

namespace App\Http\Controllers;

use App\Models\Problem;
use App\Http\Requests\ProblemRequest;

class ProblemController extends Controller
{
    /**
     * @var
     */
    private $problem;

    /**
     * @param Problem $problem
     */
    public function __construct (
        Problem $problem
    )
    {
        $this->problem = $problem;
    }   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $problems = $this->problem::paginate(15);

        return view('admin.problems.list', ['problems' => $problems]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProblemRequest $request)
    {
        $this->problem::create($request->except('_token'));

        return redirect()->back()->with('problem_success', 'Вопрос успешно добавлен!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\problem  $problem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $problem = $this->problem::findOrFail($id);

        return view('admin.problems.show', ['problem' => $problem]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\problem  $problem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->problem::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Вопрос успешно удален!');
    }
}