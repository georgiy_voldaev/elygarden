<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use App\Http\Requests\ProductCategoryStoreRequest;
use App\Http\Requests\ProductCategoryUpdateRequest;

class ProductCategoryController extends Controller
{
    /**
     * @var
     */
    private $productCategory;

    /**
     * @param ProductCategory $productCategory
     */
    public function __construct (
        ProductCategory $productCategory
    )
    {
        $this->productCategory = $productCategory;
    }   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productCategories = $this->productCategory::paginate(15);

        return view('admin.product_categories.list', ['productCategories' => $productCategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productCategories = $this->productCategory::all();
        
        return view('admin.product_categories.create',
            [
                'productCategories' => $productCategories,   
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryStoreRequest $request)
    {
        $this->productCategory::create($request->except('_token'));

        return redirect()->route('product_category.index')->with('message', 'Новая категория продукта успешно добавлена!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\productCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productCategory = $this->productCategory::findOrFail($id);
        $productCategories = $this->productCategory::where('id', '!=', $id)->where('id', '!=', $productCategory->parent_id)->get();

        return view('admin.product_categories.edit', 
            [
                'productCategory' => $productCategory,
                'productCategories' => $productCategories,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\productCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCategoryUpdateRequest $request, $id)
    {
        $this->productCategory::findOrFail($id)->update($request->except('_token'));

        return redirect()->route('product_category.index')->with('message', 'Категория продукта успешно изменена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\productCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->productCategory::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Категория продукта успешно удалена!');
    }
}
