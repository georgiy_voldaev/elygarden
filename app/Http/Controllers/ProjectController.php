<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Project;
use App\Models\CommonImage;
use App\Http\Requests\ProjectUpdateRequest;
use App\Http\Requests\ProjectStoreRequest;

class ProjectController extends Controller
{
    /**
     * @var
     */
    private $project;
    
    /**
     * @var
     */
    private $service;
    
    /**
     * @var
     */
    private $commonImage;

    /**
     * 
     * @param Project $project
     * @param Service $service
     */
    public function __construct (
        Project $project,
        CommonImage $commonImage,
        Service $service
    )
    {
        $this->project = $project;
        $this->service = $service;
        $this->commonImage = $commonImage;
    }   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = $this->project::orderBy('created_at','desc')->simplePaginate(15);
        $services = $this->service::all();

        return view('site.projects.index', 
            [
                'projects' => $projects,
                'services' => $services,
            ]
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    /**
     * Display the specified resource.
     *
     * @param  \App\Project $news
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = $this->project::where('slug', '=', $id)->first();
        $recommendedProjects = $this->project::take(15)->get()->shuffle()->take(3);

        return view('site.projects.item', 
            [
                'project' => $project,
                'recommendedProjects' => $recommendedProjects,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = $this->service::all();

        return view('admin.projects.create', ['services' => $services]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectStoreRequest $request)
    {
        $project = $this->project->create($request->except('_token'));
        
        $categories = $request->get('categories');
        foreach ($categories as $item) {
            $project->categories()->attach($item);            
        }

        foreach ($request->file('images') as $item) {
            $this->commonImage->create(
                [
                    'img' => $item,
                    'entity_id' => $project->id,
                    'entity_type' => 'project',
                ]
            );
        }

        return redirect()->route('project.listing')->with('message', 'Новый проект успешно добавлен!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
        $projects = $this->project::paginate(15);

        return view('admin.projects.list', ['projects' => $projects]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = $this->project::findOrFail($id);
        $services = $this->service::all();

        return view('admin.projects.edit', 
            [
                'project' => $project,
                'services' => $services,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectUpdateRequest $request, $id)
    {
        $project = $this->project::findOrFail($id);
        $project->update($request->except(['_token']));
        
        if ($request->get('categories')) {
            $categories = $request->get('categories');
          	$project->categories()->sync($categories);
        }

        if ($request->file('images')) {
            foreach ($request->file('images') as $file) {
                $this->commonImage->create(
                    [
                        'img' => $file,
                        'entity_id' => $project->id,
                        'entity_type' => 'project',
                    ]
                );
            }
        }

        return redirect()->route('project.listing')->with('message', 'Проект успешно изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->project::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Проект успешно удален!');
    }
}
