<?php

namespace App\Http\Controllers;

use App\Http\Requests\LicenseStoreRequest;
use App\Http\Requests\LicenseUpdateRequest;
use App\Models\License;

class LicenseController extends Controller
{
    /**
     * @var
     */
    private $license;

    /**
     * @param License $license
     */
    public function __construct (
        License $license
    )
    {
        $this->license = $license;
    }   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $licenses = $this->license::paginate(15);

        return view('admin.licenses.list', ['licenses' => $licenses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LicenseStoreRequest $request)
    {
        $this->license::create($request->except('_token'));

        return redirect()->route('license.index')->with('message', 'Новая лицензия успешно добавлена!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\License $license
     * @return \Illuminate\Http\Response
     */
    public function update(LicenseUpdateRequest $request, $id)
    {
        $this->license::findOrFail($id)->update($request->except('_token'));

        return redirect()->route('license.index')->with('message', 'Лицензия успешно изменена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\License $license
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->license::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Лицензия успешно удалена!');
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\License $license
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $license = $this->license::findOrFail($id);

        return view('admin.licenses.edit', ['license' => $license]);
    }


     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.licenses.create');
    }
}
