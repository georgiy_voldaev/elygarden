<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Attribute;
use App\Models\AttributeType;
use App\Models\CommonImage;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Requests\ProductStoreRequest;

use Illuminate\Http\Request;

use DB;

class ProductController extends Controller
{
    /**
     * @var
     */
    private $product;

    /**
     * @var
     */
    private $productCategory;

    /**
     * @var
     */
    private $attribute;

    /**
     * @var
     */
    private $attributeType;

    /**
     * @var
     */
    private $commonImage;

    /**
     * @param product $product
     */
    public function __construct (
        Product $product,
        ProductCategory $productCategory,
        Attribute $attribute,
        AttributeType $attributeType,
        CommonImage $commonImage
    )
    {
        $this->product = $product;
        $this->productCategory = $productCategory;
        $this->attribute = $attribute;
        $this->attributeType = $attributeType;
        $this->commonImage = $commonImage;
    }   
    
    private function getViewType(string $default, string $current) : string
    {
        if (isset($_GET[$current])){
            $default = $_GET[$current];
        }

        return $default;
    }

    private function getActiveAttributes ()
    {
        $activeAttributes = [];
        if (isset($_GET['attribute'])) {
            $activeAttributes = $this->attribute::whereIn('id', $_GET['attribute'])->get();
        }

        return $activeAttributes;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!isset($_GET['category']))
        {
            $categories = $this->productCategory::main()->get();
            
            return view('site.catalog.index_categories', 
                [
                    'categories' => $categories
                ]
            );
        } 
        else {
            $category = $this->productCategory::findOrFail($_GET['category']);
            if ($category->subCategories()->count() > 0) {
                $categories = $category->subCategories;

                return view('site.catalog.index_categories', 
                    [
                        'category' => $category,
                        'categories' => $categories,
                    ]
                );              
            } 
            else {
                $category = $this->productCategory::with(['products', 'attributeTypes'])->findOrFail($_GET['category']);
                $sort = $this->getViewType('increase', 'sort');
                $view = $this->getViewType('card', 'view');

                $activeAttributes = $this->getActiveAttributes();
                $products = $this->getProductByAttributes($category, $sort, $activeAttributes);  
                
              	//dd($products['products']->hasMore);
              	//$attr = $request->all(); 
              	//$attr['page'] = 4;
              	//dd($attr);
              
                $attributeTypes = $category->attributeTypes;
                
                return view('site.catalog.index', 
                    [
                        'view' => $view,
                        'category' => $category,
                        'attributeTypes' => $attributeTypes,
                        'activeAttributes' => $activeAttributes,
                        'numProducts' => $products['numProducts'],
                        'maxPrice' => $products['maxPrice'],
                        'minPrice' => $products['minPrice'],
                        'products' => $products['products'],
                    ]
                );
            }    
        }
    }

    private function getProductIdsByAttributes ($attributes) : array
    {
        $productIds = [];
        if ($attributes) {
            foreach ($attributes as $item) {
                $tmpAttribute = DB::table('product_attribute')->distinct()->where('attribute_id', '=', $item->id)->get();
                if (count($tmpAttribute) > 1) {
                    foreach ($tmpAttribute as $item) {
                        $productIds[] = $item->product_id;
                    }
                } else {
                    if (!empty($tmpAttribute->first())) {
                        $productIds[] = $tmpAttribute->first()->product_id;
                    }
                }
            }
            array_unique($productIds);
        } 

        return $productIds;
    }

    private function getProductByAttributes ($category, string $sort, $attributes) : array
    {
        $productIds = $this->getProductIdsByAttributes($attributes);
        $numProducts = $category->products()->count();     

        $minCost = null;
        if (isset($_GET['min_cost'])){
            $minCost = $_GET['min_cost'];
        }

        $maxCost = null;
        if (isset($_GET['max_cost'])){
            $maxCost = $_GET['max_cost'];
        }
        
        $products = $category->products();

        $maxPrice = (int) $products->max('cost');
        $minPrice = (int) $products->min('cost');
        $products =  $products->when($minCost != null, function ($query) use ($minCost, $maxCost) {
                                    return $query->whereBetween('cost', [$minCost, $maxCost]);
                                })
                                ->when($sort == 'increase', function ($query) {
                                    return $query->orderBy("cost");
                                })
                                ->when($sort == 'decrease', function ($query) {
                                    return $query->orderBy("cost", 'DESC');
                                })
                                ->when($productIds !== [], function ($query) use ($productIds) {
                                    return $query->whereIn('id', $productIds);
                                })
                                ->simplePaginate(15);

        return [
            'numProducts' => $numProducts,
            'products' => $products,
            'maxPrice' => $maxPrice,
            'minPrice' => $minPrice,
        ];
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
        $products = $this->product::paginate(15);

        return view('admin.products.list', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productCategories = $this->productCategory::all();
        $attributes = $this->attribute::all();
        $attributeTypes = $this->attributeType::all();

        return view('admin.products.create', 
            [
                'productCategories' => $productCategories,
                'attributes' => $attributes,
                'attributeTypes' => $attributeTypes,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $product = $this->product::create($request->except(['_token', 'attributes', 'images']));

        $attributes = [];
        foreach($request->get('attributes') as $item){
            if ($item != null)
            {
                $attributes[] = $item;
            }
        }
        $product->attributes()->sync($attributes);                            

        foreach ($request->file('images') as $item) {
            $this->commonImage->create(
                [
                    'img' => $item,
                    'entity_id' => $product->id,
                    'entity_type' => 'product',
                ]
            );
        }

        return redirect()->route('product.listing')->with('message', 'Новый продукт успешно добавлен!');
    }
    
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product::findOrFail($id);
        $productRecommended = $this->product::take(15)->get()->shuffle()->take(3);

        $links = DB::table('product_attribute')->where('product_id', '=', $product->id)->get();
        $attributesId = [];
        foreach ($links as $item) {
            $attributesId[] = $item->attribute_id;
        }

        $attributes = $this->attribute::find($attributesId);

        return view('site.catalog.item', 
            [
                'product' => $product,
                'productRecommended' => $productRecommended,
                'attributes' => $attributes,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->product::findOrFail($id);
        $productCategories = $this->productCategory::all();
        $attributes = $this->attribute::all();
        $attributeTypes = $this->attributeType::all();

        return view('admin.products.edit', 
            [
                'product' => $product,
                'productCategories' => $productCategories,
                'attributes' => $attributes,
                'attributeTypes' => $attributeTypes,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $product = $this->product::findOrFail($id);
        $product->update($request->except('_token', 'attributes', 'images'));

        $attributes = [];
        foreach($request->get('attributes') as $item){
            if ($item != null)
            {
                $attributes[] = $item;
            }
        }

        $product->attributes()->sync($attributes);                            
       
        if ($request->file('images')) {
            foreach ($request->file('images') as $item) {
                $this->commonImage->create(
                    [
                        'img' => $item,
                        'entity_id' => $product->id,
                        'entity_type' => 'product',
                    ]
                );
            }
        }

        return redirect()->route('product.listing')->with('message', 'Продукт успешно изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('product_attribute')->where(
            'product_id', '=', $id
        )->delete();
        $this->product::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Продукт успешно удален!');
    }
}
