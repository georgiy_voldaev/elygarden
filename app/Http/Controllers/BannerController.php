<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Http\Requests\BannerStoreRequest;
use App\Http\Requests\BannerUpdateRequest;

class BannerController extends Controller
{
    /**
     * @var
     */
    private $banner;

    /**
     * @param banner $banner
     */
    public function __construct (
        Banner $banner
    )
    {
        $this->banner = $banner;
    }   
  
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = $this->banner::paginate(15);

        return view('admin.banners.list', ['banners' => $banners]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerStoreRequest $request)
    {
        $this->banner::create($request->except('_token'));

        return redirect()->route('banner.index')->with('message', 'Новый баннер успешно добавлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\License $license
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->banner::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Баннер успешно удален!');
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banners.create');
    }

     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = $this->banner::findOrFail($id);

        return view('admin.banners.edit', ['banner' => $banner]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(BannerUpdateRequest $request, $id)
    {
        $this->banner::findOrFail($id)->update($request->except('_token'));

        return redirect()->route('banner.edit', ['id' => 1])->with('message', 'Баннер успешно изменен!');
    }
}
