<?php

namespace App\Http\Controllers;

use App\Models\CommonSeo;
use App\Http\Requests\CommonSeoRequest;

class CommonSeoController extends Controller
{
    /**
     * @var
     */
    private $commonSeo;

    /**
     * @param CommonSeo $commonSeo
     */
    public function __construct (
        CommonSeo $commonSeo
    )
    {
        $this->commonSeo = $commonSeo;
    }   
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\commonSeo  $commonSeo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $commonSeo = $this->commonSeo::findOrFail($id);

        return view('admin.common_seo.edit', ['commonSeo' => $commonSeo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\commonSeo  $commonSeo
     * @return \Illuminate\Http\Response
     */
    public function update(CommonSeoRequest $request, $id)
    {
        $this->commonSeo::findOrFail($id)->update($request->except('_token'));

        return redirect()->back()->with('message', 'Общие SEO настройки успешно изменены!');
    }
}
