<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\AttributeType;
use App\Models\ProductCategory;
use App\Http\Requests\AttributeRequest;

class AttributeController extends Controller
{
    /**
     * @var
     */
    private $attribute;

    /**
     * @var
     */
    private $productCategory;

    /**
     * @var
     */
    private $attributeType;

    /**
     * @param attribute $attribute
     */
    public function __construct (
        Attribute $attribute,
        AttributeType $attributeType,
        ProductCategory $productCategory
    )
    {
        $this->attribute = $attribute;
        $this->attributeType = $attributeType;
        $this->productCategory = $productCategory;
    }   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributes = $this->attribute::paginate(15);

        return view('admin.attributes.list', ['attributes' => $attributes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attributeTypes = $this->attributeType::all();
        $productCategories = $this->productCategory::all();

        return view('admin.attributes.create', 
            [
                'attributeTypes' => $attributeTypes,
                'productCategories' => $productCategories,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeRequest $request)
    {
        $this->attribute::create($request->except('_token'));

        return redirect()->route('attribute.index')->with('message', 'Новый атрибут успешно добавлен!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attribute = $this->attribute::findOrFail($id);
        $attributeTypes = $this->attributeType::all();
        $productCategories = $this->productCategory::all();

        return view('admin.attributes.edit', 
            [
                'attribute' => $attribute, 
                'attributeTypes' => $attributeTypes,
                'productCategories' => $productCategories,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(AttributeRequest $request, $id)
    {
        $this->attribute::findOrFail($id)->update($request->except('_token'));

        return redirect()->route('attribute.index')->with('message', 'Атрибут успешно изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->attribute::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Атрибут успешно удален!');
    }
}
