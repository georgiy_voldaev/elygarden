<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Project;
use App\Models\CommonImage;
use App\Http\Requests\ServiceUpdateRequest;
use App\Http\Requests\ServiceStoreRequest;

class ServiceController extends Controller
{
    /**
     * @var
     */
    private $service;
    
    /**
     * @var
     */
    private $project;

    /**
     * @var
     */
    private $commonImage;

    /**
     * @param news $news
     */
    public function __construct (
        Service $service,
        Project $project,
        CommonImage $commonImage
    )
    {
        $this->service = $service;
        $this->project = $project;
        $this->commonImage = $commonImage;
    }   
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = $this->service::orderBy('created_at')->simplePaginate(10);

        return view('site.services.index', ['services' => $services]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show ($id)
    {
        $service = $this->service::where('slug', '=', $id)->first();
        $projects = $service->projects;
        
        return view('site.services.item',
            [
                'service' => $service,
                'projects' => $projects,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceStoreRequest $request)
    {
        $service = $this->service->create($request->except('_token'));

        foreach ($request->file('images') as $item) {
            $this->commonImage->create(
                [
                    'img' => $item,
                    'entity_id' => $service->id,
                    'entity_type' => 'service',
                ]
            );
        }

        return redirect()->route('service.listing')->with('message', 'Услуга успешно добавлена!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
        $services = $this->service::paginate(15);

        return view('admin.services.list', ['services' => $services]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = $this->service::findOrFail($id);

        return view('admin.services.edit', ['service' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceUpdateRequest $request, $id)
    {
        $service = $this->service::findOrFail($id);
        $service->update($request->except(['_token']));
        
        if ($request->file('images')) {
            foreach ($request->file('images') as $file) {
                $this->commonImage->create(
                    [
                        'img' => $file,
                        'entity_id' => $service->id,
                        'entity_type' => 'service',
                    ]
                );
            }
        }

        return redirect()->route('service.listing')->with('message', 'Услуга успешно изменена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->service::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Услуга успешно удалена!');
    }
}
