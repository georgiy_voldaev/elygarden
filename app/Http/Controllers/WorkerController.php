<?php

namespace App\Http\Controllers;

use App\Models\Worker;
use App\Http\Requests\WorkerUpdateRequest;
use App\Http\Requests\WorkerStoreRequest;

class WorkerController extends Controller
{
    /**
     * @var
     */
    private $worker;

    /**
     * @param Worker $Worker
     */
    public function __construct (Worker $worker)
    {
        $this->worker = $worker;
    }   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers = $this->worker::paginate(15);

        return view('admin.workers.list', ['workers' => $workers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.workers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkerStoreRequest $request)
    {
        $this->worker::create($request->except('_token'));

        return redirect()->route('worker.index')->with('message', 'Новый работник успешно добавлен!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Worker  $Worker
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $worker = $this->worker::findOrFail($id);

        return view('admin.workers.edit', ['worker' => $worker]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Worker  $Worker
     * @return \Illuminate\Http\Response
     */
    public function update(WorkerUpdateRequest $request, $id)
    {
        $this->worker::findOrFail($id)->update($request->except('_token'));

        return redirect()->route('worker.index')->with('message', 'Работник успешно изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worker  $Worker
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->worker::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Работник успешно удален!');
    }
}
