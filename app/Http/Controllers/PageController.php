<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Service;
use App\Models\Worker;

class PageController extends Controller
{
    /**
     * @var
     */
    private $banner;

    /**
     * @var
     */
    private $service;

    /**
     * @var
     */
    private $review;

    /**
     * @param AttributeType $attributeType
     */
    public function __construct (
        Banner $banner,
        Worker $worker,
        Service $service
    )
    {
        $this->banner = $banner;
        $this->service = $service;
        $this->worker = $worker;
    }   

	/**
     * Undocumented function
     *
     * @return void
     */
    public function main ()
    {
        $banners = $this->banner::all();
        $services = $this->service::all();
        
        return view('site.main.index',
            [
                'banners' => $banners,
                'services' => $services,
            ]
        );
    }
	
	/**
     * Undocumented function
     *
     * @return void
     */
    public function about ()
    {
        $workers = $this->worker::all();

        return view('site.about.index', 
            [
                'workers' => $workers,
            ]
        );
    }
	
	/**
     * Undocumented function
     *
     * @return void
     */
    public function contacts ()
    {
        return view('site.contacts.index');
    }
	
  	/**
     * Undocumented function
     *
     * @return void
     */
  	public function cookie ()
    {
  		return view('site.cookie.index');    
    }
  
	/**
     * Undocumented function
     *
     * @return void
     */
    public function policy ()
    {
        return view('site.policy.index');
    }
}
