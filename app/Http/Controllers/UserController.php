<?php

namespace App\Http\Controllers;

use Auth;

use App\Models\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
     /**
     * @var
     */
    private $user;

    /**
     * @param User $user
     */
    public function __construct (
        User $user
    )
    {
        $this->user = $user;
    }   
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user::paginate(15);

        return view('admin.users.list', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $this->user::create($request->except('_token'));

        return redirect()->route('user.index')->with('message', 'Новый пользователь успешно добавлен!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user::findOrFail($id);

        return view('admin.users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $this->user::findOrFail($id)->update($request->except('_token'));

        return redirect()->route('user.index')->with('message', 'Пользователь успешно изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user::findOrFail($id)->delete();

        return redirect()->back()->with('message', 'Пользователь успешно удален!');
    }

   
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];
       
        if (Auth::attempt(['email' => $email, 'password' => $password]))
        {
            return redirect()->route('user.edit', ['id' => 1]);
        }else {
            return redirect()->back();
        }
    }


    public function loginPage()
    {
        return view('admin.enter');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
