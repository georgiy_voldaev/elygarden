<?php

namespace App\Http\Controllers;

use App\Models\CommonImage;
use Illuminate\Http\Request;

class CommonImageController extends Controller
{
	private $commonImage;
    
    /**
     * @param banner $banner
     */
    public function __construct (CommonImage $commonImage)
    {
        $this->commonImage = $commonImage;
    }   

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function destroy ($id)
    {
    	$this->commonImage->findOrFail($id)->delete();
        
        return redirect()->back()->with('message', 'Изображение удалено!');
    }
}