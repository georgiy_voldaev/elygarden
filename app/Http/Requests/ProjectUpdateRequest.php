<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:255',
            'img' => 'image|max:10240',
            'description' => 'required|string',
            'short_description' => 'required|max:255|string',
            'seo_title' => 'required|max:255|string',
            'seo_description' => 'required|max:255|string',
            'seo_keywords' => 'required|max:255|string',
            'location' => 'required|max:255|string',
            'og_vk_image' => 'image|max:10240',
        
            'images' => 'array',
            'categories' => 'array',
        ];
    }
}
