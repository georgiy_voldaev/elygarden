<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommonSeoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'about_title' => 'required|max:255|string',
            'about_description' => 'required|max:255|string',
            'about_keywords' => 'required|max:255|string',

            'contacts_title' => 'required|max:255|string',
            'contacts_description' => 'required|max:255|string',
            'contacts_keywords' => 'required|max:255|string',
            
            'catalog_title' => 'required|max:255|string',
            'catalog_description' => 'required|max:255|string',
            'catalog_keywords' => 'required|max:255|string',

            'main_title' => 'required|max:255|string',
            'main_description' => 'required|max:255|string',
            'main_keywords' => 'required|max:255|string',

            'services_title' => 'required|max:255|string',
            'services_description' => 'required|max:255|string',
            'services_keywords' => 'required|max:255|string',

            'projects_title' => 'required|max:255|string',
            'projects_description' => 'required|max:255|string',
            'projects_keywords' => 'required|max:255|string',
        ];
    }
}
