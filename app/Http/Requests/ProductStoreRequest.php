<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:255|unique:products',
            'old_cost' => 'required|between:0.00,999999.99',
            'cost' => 'required|between:0.00,999999.99',
            'description' => 'required|max:255|string',
            'short_description' => 'required|string',
            'seo_title' => 'required|max:255|string',
            'seo_description' => 'required|max:255|string',
            'seo_keywords' => 'required|max:255|string',
            'seo_vk_image' => 'required|image|max:10240',
            'category_id' => 'required',
        
            'attributes' => 'required|array',
            'images' => 'required|array',
        ];
    }
}
