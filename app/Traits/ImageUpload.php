<?php
namespace App\Traits;

use function Couchbase\defaultDecoder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Image;

/**
 * Trait ImageUpload
 * @package App\Traits
 */
trait  ImageUpload
{
    /**
     * @var
     */
    private $file;

    /**
     * @var
     */
    private $type;


    /**
     * @var
     */
    private $defaultPath;

    /**
     * @param $image
     */
    private function setFile ($image, $ext = '')
    {
        if ($ext == '') {
            $this->type = $image->getClientOriginalExtension();
        } else {
            $this->type = 'jpg';
        }
            
        $this->file = Image::make($image);
        $this->defaultPath = $this->setPath('default');
    }

    /**
     * @param $variant
     * @return string
     */
    private function setPath ($variant) : string
    {
        $fileName = md5(microtime() . rand(0, 9999));

        return 'public/'.substr($fileName, 0,2).'/'
                      .substr($fileName, 2,2).'/'
                      .$fileName.'/'.$variant.'.'.$this->type;
    }

    /**
     * @param string $path
     * @param array $variant
     */
    private function saveItem (string $path, array $variant)
    {
        $this->file->backup();
        if (isset($variant['height'])) {
            $image = $this->file->fit($variant['width'], $variant['height']);
        } else {
            $image = $this->file->fit($variant['width']);
        }
        $image->encode($this->type, 70);

        Storage::put($path, $image);
        $this->file->reset();
    }

    /**
     * @param string $path
     * @param string $variant
     */
    private function deleteImageFile (string $path, string $variant)
    {
        Storage::delete(str_replace('default', $variant, $path));
    }

    /**
     * @param array $variants
     * @param $file
     * @param string|null $oldPath
     * @return string
     */
    public function saveFiles (array $variants, $file, string $oldPath = null, $ext = '') : string
    {
        $this->setFile($file, $ext);
        $path = '';
        foreach ($variants as $variant) {
            if (empty($oldPath)) {
                $path = str_replace('default', $variant['name'], $this->defaultPath);
                $this->saveItem($path, $variant);
            } else {
                $path = str_replace('default', $variant['name'], $oldPath);
                $this->saveItem($path, $variant);
            }
        }
        
        return $oldPath == null ? $this->defaultPath : $oldPath;
    }

    /**
     * @param array $variants
     * @param string $path
     */
    protected function deleteAll (array $variants, $path)
    {
        if (empty($path))
        {
            Log::info('Вот кое-какая полезная информация.');
        } else {
            foreach ($variants as $variant) {
                $this->deleteImageFile($path, $variant['name']);
            }
        }
    }
}