// owl carousel for item news and item excursion
$(document).ready(function(){
    $(".main-slider").owlCarousel({
        responsive: {
            0: {
                items: 1,
                dots: false
            },
            768: {
                items: 1,
                dots: true
            }
        },
        loop: true,
        center: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: true,
        nav: true,
        navText: ['<span class="slide-nav slide-nav-left"></span>', '<span class="slide-nav slide-nav-right"></span>' ]
    });

});

$(document).ready(function(){
    $(".item-slider").owlCarousel({
        responsive: {
            0: {
                items: 1,
                dots: false
            },
            768: {
                items: 1.5,
                dots: true
            }
        },
        loop: true,
        margin: 10,
        center: true,
        nav: true,
        navText: ['<span class="slide-nav slide-nav-left"></span>', '<span class="slide-nav slide-nav-right"></span>' ]
    });
});

$(document).ready(function(){    
    $(".wrap-reviews").owlCarousel({
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            960: {
                items: 4
            }
        },
        loop: true,
        margin: 25,
        nav: true,
        navText: ['<span class="navigation-slider slide-nav-left-prod"></span>', '<span class="navigation-slider slide-nav-right-prod"></span>' ]
    });
});

$(document).ready(function(){
    $(".product-slider").owlCarousel({
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 1,
            }
        },
        loop: true,
        center: true,
        URLhashListener:true,
        startPosition: 'URLHash',
        nav: true,
        navText: ['<span class="navigation-slider slide-nav-left-prod"></span>', '<span class="navigation-slider slide-nav-right-prod"></span>' ]
    });
});

$(document).ready(function(){
    $( ".slider" ).slider({
        animate: "fast",
        range: true,
        step: 1,
        max: $(".slider").data('max'),
        min: $(".slider").data('min'),
        values: [$(".slider").data('left'), $(".slider").data('right')]
    });

    $(".slider").on("slide", function( event, ui ) {
        $(".min-value").val($(this).slider('values')[0]);
        $(".max-value").val($(this).slider('values')[1]);    
    });

    $(".slider").on( "slidestop", function( event, ui ) {
        var values = $(this).slider("values");
        var replaceTextMin = '&min_cost=' + $(this).data('left');
        var replaceTextMax = '&max_cost=' + $(this).data('right');
        var href = location.href.replace(replaceTextMin, '').replace(replaceTextMax, '');
        var newLink = href + '&min_cost=' + values[0] + '&max_cost=' + values[1];
        location.href = newLink; 
    });

    
    $('.min-value').on('change', function () {
        $( ".slider" ).slider("values", 0, [$(this).val()]);
        var values = $( ".slider" ).slider("values");
        var replaceTextMin = '&min_cost=' + $(".slider").data('left');
        var replaceTextMax = '&max_cost=' + $(".slider").data('right');
        var href = location.href.replace(replaceTextMin, '').replace(replaceTextMax, '');
        var newLink = href + '&min_cost=' + values[0] + '&max_cost=' + values[1];
        location.href = newLink; 
    });

    $('.max-value').on('change', function () {
        $( ".slider" ).slider("values", 1, [$(this).val()]);
        var values = $( ".slider" ).slider("values");
        var replaceTextMin = '&min_cost=' + $(".slider").data('left');
        var replaceTextMax = '&max_cost=' + $(".slider").data('right');
        var href = location.href.replace(replaceTextMin, '').replace(replaceTextMax, '');
        var newLink = href + '&min_cost=' + values[0] + '&max_cost=' + values[1];
        location.href = newLink; 
    });
});

$(document).ready(function(){
    $('.filter-close').on('click', function () {
        UIkit.offcanvas.hide();
    });
});

$(document).ready(function () {
    if (window.innerWidth > 1100) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-default'); } );
    } else if ((window.innerWidth <= 1100) && (window.innerWidth > 600)) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-middle'); } );
    } else if (window.innerWidth <= 600) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-mini'); } );
    }
});

$(window).resize(function () {
    if (window.innerWidth > 1100) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-default'); } );
    } else if ((window.innerWidth <= 1100) && (window.innerWidth > 600)) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-middle'); } );
    } else if (window.innerWidth <= 600) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-mini'); } );
    }
});