$(document).ready(function () {
    if (window.innerWidth > 1100) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-default'); } );
    } else if ((window.innerWidth <= 1100) && (window.innerWidth > 600)) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-middle'); } );
    } else if (window.innerWidth <= 600) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-mini'); } );
    }
});

$(window).resize(function () {
    if (window.innerWidth > 1100) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-default'); } );
    } else if ((window.innerWidth <= 1100) && (window.innerWidth > 600)) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-middle'); } );
    } else if (window.innerWidth <= 600) {
        $('.change-img').each( function(){ this.src = this.getAttribute('data-mini'); } );
    }
});

$(document).ready(function(){
    $(".main-slider").owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        autoplay: true,
        autoplayHoverPause: true,
        navText: ['<span class="slide-nav main-slide-nav slide-nav-left"></span>', '<span class="slide-nav main-slide-nav slide-nav-right"></span>' ]
    });
});

$(document).ready(function(){
    $(".item-slider").owlCarousel({
        responsive: {
            0: {
                items: 1,
                dots: false
            },
            768: {
                items: 1.5,
                dots: true
            }
        },
        loop: true,
        margin: 10,
        center: true,
        nav: true,
        navText: ['<span class="slide-nav slide-nav-left"></span>', '<span class="slide-nav slide-nav-right"></span>' ]
    });
});

$(document).ready(function(){
    var el = $('.main-nav');
    if (el.offset().top !== 0) {
        el.removeClass('scroll-height');
    } else {
        el.addClass('scroll-height');
    }
    $(document).scroll(function () {
        var el = $('.main-nav');
        if (el.offset().top > 120) {
            el.removeClass('scroll-height');
        } else {
            el.addClass('scroll-height');
        }
    });
});

$(document).ready(function () {
    $('.btn-open-mob-menu').on('click', function (){
        $('.mob-effect').addClass('blur-effect');
        $('.mobile-nav').addClass('mobile-nav-active');
    });
});

$(document).ready(function () {
    $('.mob-menu-close').on('click', function (){
        $('.mobile-nav').removeClass('mobile-nav-active');
        $('.mob-effect').removeClass('blur-effect');
    });
});