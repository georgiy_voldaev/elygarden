function handleFileSelect(evt) {
    var files = evt.target.files;

    for (var i = 0, f; f = files[i]; i++) {
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();
        reader.onload = (function(theFile) {
            return function(e) {
                var id = Math.random() * (100000 - 1) + 1;
                $('#files').attr('id', id).attr('style', 'display:none;');
                var client = '<li class="list-unstyled racurs-list-item col-sm-4">' +
                                '<div class="uk-overlay uk-width-1-1">' +
                                    '<img class="img-thumbnail img-responsive" src="'+e.target.result+'">' +
                                '</div>' +
                                '<button data-id="'+id+'" class="col-sm-12 delete-img btn btn-danger">Удалить</button>'+
                              '</li>';
                $('.uk-list').append(client);

                $('.delete-img').on('click', function() {
                    document.getElementById($(this).data('id')).remove();
                    $(this).parent().remove();
                });

                $('<input id="files" class="input-dwnld-view-photo" type="file" name="images[]">').appendTo('.wrap-dwnld-photo');

                $('#files').on('change', handleFileSelect);
            };
        })(f);

        reader.readAsDataURL(f);
    }
}

function imageChange(evt) {
    var files = evt.target.files;

    for (var i = 0, f; f = files[i]; i++) {

        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();
        reader.onload = (function(theFile) {
            return function(e) {
                $(this).parent().prev().attr('src', e.target.result);
                $(this).parent().prev().addClass('col-sm-6');
            };
        })(f, this);
        reader.readAsDataURL(f);
    }
}
$('.one-img-upload').on('change', function (e) {
    var file = e.target.files[0];
    var reader = new FileReader();

    reader.readAsDataURL(file);
    var img = $(this).parent().prev();
    reader.onload = function(e) {
        img.attr('src', reader.result).addClass('col-sm-12');
    }

});

$('#files').on('change', handleFileSelect);
