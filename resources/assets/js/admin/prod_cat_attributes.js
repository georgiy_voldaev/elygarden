$(document).ready(function(){
    var catId = $("#category").val();
    console.log(catId);
    $("#attributes").children('.attributes-wrap').fadeOut();
    $("#attributes").children('.attributes-wrap-'+catId).fadeIn();

    $("#category").on('change', function () {
        var catId = $("#category").val();
        console.log(catId);
        $("#attributes").children('.attributes-wrap').fadeOut();
        $("#attributes").children('.attributes-wrap-'+catId).fadeIn();
    });
});