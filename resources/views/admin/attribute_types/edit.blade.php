@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Редактировать тип атрибута
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'PATCH', 'url' => route('attribute_type.update', ['id' => $attributeType->id])]) }}
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Название</label>
                            <input name="name" type="text" class="form-control" value="{{ $attributeType->name }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                    <div class="col-sm-6 col-sm-offset-3">
                        <select multiple class="form-control show-tick" name="category_id[]">
                            @foreach($attributeType->categories as $category)
                                <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                            @foreach($categories as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Редактировать">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
