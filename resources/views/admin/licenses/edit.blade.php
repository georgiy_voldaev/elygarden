@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Редактировать лицензию
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'PATCH', 'files' => true, 'url' => route('license.update', ['id' => $license->id])]) }}
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Подпись</label>
                            <input name="title" type="text" class="form-control" value="{{ $license->title }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">Изображение</label>
                        <img src="{{ Storage::url($license->img) }}" class="img-responsive img-thumbnail">
                        <div class="btn-group bootstrap-select form-control show-tick">
                            <input class="one-img-upload" type="file" name="img">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Редактировать  ">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop