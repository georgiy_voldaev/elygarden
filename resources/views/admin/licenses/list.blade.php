@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Список лицензий
            </h2>
            <ul class="header-dropdown m-r--5">
                <a href="{{route('license.create')}}" class="btn btn-primary waves-effect">Добавить лицензию</a>
            </ul>
        </div>
        <div class="body table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Изображение</th>
                    <th>Подпись</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($licenses as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td><img class="img-thumbnail col-sm-2" src="{{ Storage::url($item->img('default', 'img')) }}"></td>
                        <td>{{ $item->title }}</td>
                        <td>
                            {{ Form::open(['method' => 'DELETE', 'url' => route('license.destroy', ['id' => $item->id])]) }}
                                <button class="col-sm-offset-3 btn btn-danger waves-effect col-sm-5" type="submit">Удалить</button>
                            {{ Form::close() }}
                            <a href="{{route('license.edit', ['id' => $item->id]) }}" class="col-sm-offset-3 btn btn-warning waves-effect col-sm-5">Редактировать</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $licenses->render() }}
        </div>
    </div>
@stop