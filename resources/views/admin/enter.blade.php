<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Admin Panel</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="{{asset('css/admin.css')}}" rel="stylesheet" />
</head>
<body class="theme-indigo login-page ls-closed">
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);">ELYGARDEN</b></a>
        <small>Административная панель</small>
    </div>
    <div class="card">
        <div class="body">
            {{ Form::open(['method' => 'POST', 'url' => route('user.login')]) }}
                <div class="msg">Чтобы войти введите логин и пароль</div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="email" placeholder="Логин" required="" autofocus="" aria-required="true">
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Пароль" required="" aria-required="true">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <button class="btn btn-block bg-pink waves-effect" type="submit">Войти</button>
                    </div>
                </div>
            {{Form::close()}}
        </div>
    </div>
</div>
<script src="{{asset('js/admin.js')}}"></script>
</body>
</html>