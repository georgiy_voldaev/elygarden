@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Редактировать проект
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'POST', 'files' => true, 'url' => 'admin/project_store/'.$project->id]) }}
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Название</label>
                            <input name="name" type="text" class="form-control" value="{{ $project->name }}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Адрес объекта</label>
                            <input name="location" type="text" class="form-control" value="{{ $project->location }}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Текст превью</label>
                            <textarea name="short_description" rows="1" class="form-control no-resize auto-growth">{{ $project->short_description }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Текст полный</label>
                            <textarea id="description" name="description">{{ $project->description }}</textarea>
                            <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
                            <script>
                                CKEDITOR.replace( 'description' );
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">Фото превью</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($project->img('large', 'img')) }}">
                        <div class="btn-group bootstrap-select form-control show-tick">
                            <input class="one-img-upload" type="file" name="img">
                        </div>
                    </div>
                </div>
            </div>
            <div id="wrap-racourse" class="row clearfix">
                <div class="wrap-dwnld-photo col-sm-6 col-sm-offset-3">
                    <span class="add-photo-ico racurs-margin-ico uk-icon-justify uk-icon-plus"></span>
                    <span class="add-photo-text racurs-margin-text">Добавить слайды</span>
                    <input id="files" class="input-dwnld-view-photo" multiple type="file" name="images[]">
                </div>
                <ul class="col-sm-6 col-sm-offset-3 uk-list angles-list uk-grid uk-grid-width-1-3 uk-margin-remove">
                    @foreach($project->commonImages as $image)
                        <li class="list-unstyled racurs-list-item col-sm-4">
                            <div class="uk-overlay uk-width-1-1">
                                <img class="img-thumbnail img-responsive" src="{{ Storage::url($image->img('default', 'img')) }}">
                            </div>
                            <a href="{{ route('common_image.destroy', ['id' => $image->id]) }}" class="col-sm-12 delete-img btn btn-danger">Удалить</a> 
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">Категории</label>
                        <select multiple name="categories[]" class="form-control show-tick">
                            <option value="">— Please select —</option>
                            @foreach($project->categories as $item)
                                <option selected value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                            @foreach($services as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO заголовок</label>
                            <input name="seo_title" type="text" class="form-control"  value="{{ $project->seo_title }}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO описание</label>
                            <input name="seo_description" type="text" class="form-control"  value="{{ $project->seo_description }}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO ключевые слова</label>
                            <input name="seo_keywords" type="text" class="form-control"  value="{{ $project->seo_keywords }}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">SEO изображение</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($project->img('default', 'og_vk_image')) }}">
                        <div class="btn-group bootstrap-select form-control show-tick">
                            <input class="one-img-upload" type="file" name="og_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Добавить">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop