@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Добавить работника
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'POST', 'files' => true, 'url' => route('worker.store')]) }}
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Имя</label>
                            <input name="name" type="text" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Специальность</label>
                            <textarea name="text" rows="1" class="form-control no-resize auto-growth"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">Изображение</label>
                        <img class="img-responsive img-thumbnail">
                        <div class="btn-group bootstrap-select form-control show-tick">
                            <input class="one-img-upload" type="file" name="img">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Добавить">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop