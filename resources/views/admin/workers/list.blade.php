@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Список работников
            </h2>
            <ul class="header-dropdown m-r--5">
                <a href="{{route('worker.create')}}" class="btn btn-primary waves-effect">Добавить отзыв</a>
            </ul>
        </div>
        <div class="body table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Изображение</th>
                    <th>Имя</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($workers as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td><img class="img-thumbnail col-sm-2" src="{{ Storage::url($item->img('default', 'img')) }}"></td>
                        <td>{{ $item->name }}</td>
                        <td>
                            {{ Form::open(['method' => 'DELETE', 'url' => route('worker.destroy', ['id' => $item->id])]) }}
                                <button class="col-sm-offset-3 btn btn-danger waves-effect col-sm-5" type="submit">Удалить</button>
                            {{ Form::close() }}
                            <a href="{{route('worker.edit', ['id' => $item->id]) }}" class="col-sm-offset-3 btn btn-warning waves-effect col-sm-5">Редактировать</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $workers->render() }}
        </div>
    </div>
@stop