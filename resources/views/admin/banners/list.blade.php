@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Список баннеров
            </h2>
            <ul class="header-dropdown m-r--5">
                <a href="{{route('banner.create')}}" class="btn btn-primary waves-effect">Добавить баннер</a>
            </ul>
        </div>
        <div class="body table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Изображение</th>
                    <th>Заголовок</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($banners as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td><img class="img-thumbnail col-sm-2" src="{{ Storage::url($item->img('default', 'img')) }}"></td>
                        <td>{{ $item->title }}</td>
                        <td>
                            {{ Form::open(['method' => 'DELETE', 'url' => route('banner.destroy', ['id' => $item->id])]) }}
                                <button class="col-sm-offset-3 btn btn-danger waves-effect col-sm-5" type="submit">Удалить</button>
                            {{ Form::close() }}
                            <a href="{{route('banner.edit', ['id' => $item->id]) }}" class="col-sm-offset-3 btn btn-warning waves-effect col-sm-5">Редактировать</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $banners->render() }}
        </div>
    </div>
@stop