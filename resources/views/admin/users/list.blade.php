@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Список пользователей
            </h2>
            <ul class="header-dropdown m-r--5">
                <a href="{{ route('user.create') }}" class="btn btn-primary waves-effect">Добавить пользователя</a>
            </ul>
        </div>
        <div class="body table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Имя</th>
                    <th>Логин</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>
                            {{ Form::open(['method' => 'DELETE', 'url' => route('user.destroy', ['id' => $item->id])]) }}
                                <button class="col-sm-offset-3 btn btn-danger waves-effect col-sm-5" type="submit">Удалить</button>
                            {{ Form::close() }}
                            <a href="{{ route('user.edit', ['id' => $item->id]) }}" class="col-sm-offset-3 btn btn-warning waves-effect col-sm-5">Редактировать</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $users->render() }}
        </div>
    </div>
@stop