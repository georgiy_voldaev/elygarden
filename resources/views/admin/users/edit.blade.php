@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Редактировать пользователя
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'PATCH', 'url' => route('user.update', ['id' => $user->id])]) }}
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Имя</label>
                            <input value="{{ $user->name }}" name="name" type="text" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Логин</label>
                            <input value="{{ $user->email }}" type="text" name="email" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Пароль</label>
                            <input type="password" name="password"  class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Сохранить">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop