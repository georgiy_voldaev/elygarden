@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Редактировать общие seo настройки
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'PATCH', 'files' => true, 'url' => route('common_seo.update', ['id' => $commonSeo->id])]) }}
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO title главной страницы</label>
                            <input name="main_title" type="text" class="form-control" value="{{ $commonSeo->main_title }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO description главной страницы</label>
                            <textarea name="main_description" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->main_description }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO keywords главной страницы</label>
                            <textarea name="main_keywords" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->main_keywords }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">OGG изображение главной страницы</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($commonSeo->img('default', 'main_vk_image')) }}">
                        <div class="btn-group bootstrap-select form-control show-tick"  >
                            <input class="one-img-upload" type="file" name="main_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO title страницы каталога</label>
                            <input name="catalog_title" type="text" class="form-control" value="{{ $commonSeo->catalog_title }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO description страницы каталога</label>
                            <textarea name="catalog_description" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->catalog_description }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO keywords страницы каталога</label>
                            <textarea name="catalog_keywords" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->catalog_keywords }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">OGG изображение страницы каталога</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($commonSeo->img('default', 'catalog_vk_image')) }}">
                        <div class="btn-group bootstrap-select form-control show-tick"  >
                            <input class="one-img-upload" type="file" name="catalog_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO title страницы о нас</label>
                            <input name="about_title" type="text" class="form-control" value="{{ $commonSeo->about_title }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO description страницы о нас</label>
                            <textarea name="about_description" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->about_description }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO keywords страницы о нас</label>
                            <textarea name="about_keywords" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->about_keywords }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">OGG изображение страницы о нас</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($commonSeo->img('default', 'about_vk_image')) }}">
                        <div class="btn-group bootstrap-select form-control show-tick"  >
                            <input class="one-img-upload" type="file" name="about_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO title страницы контактов</label>
                            <input name="contacts_title" type="text" class="form-control" value="{{ $commonSeo->contacts_title }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO description страницы контактов</label>
                            <textarea name="contacts_description" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->contacts_description }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO keywords страницы контактов</label>
                            <textarea name="contacts_keywords" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->contacts_keywords }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">OGG изображение страницы контактов</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($commonSeo->img('default', 'contacts_vk_image')) }}">
                        <div class="btn-group bootstrap-select form-control show-tick"  >
                            <input class="one-img-upload" type="file" name="contacts_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO title страницы услуг</label>
                            <input name="services_title" type="text" class="form-control" value="{{ $commonSeo->services_title }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO description страницы услуг</label>
                            <textarea name="services_description" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->services_description }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO keywords страницы услуг</label>
                            <textarea name="services_keywords" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->services_keywords }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">OGG изображение страницы услуг</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($commonSeo->img('default', 'services_vk_image')) }}">
                        <div class="btn-group bootstrap-select form-control show-tick"  >
                            <input class="one-img-upload" type="file" name="services_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO title страницы проектов</label>
                            <input name="projects_title" type="text" class="form-control" value="{{ $commonSeo->projects_title }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO description страницы проектов</label>
                            <textarea name="projects_description" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->projects_description }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO keywords страницы проектов</label>
                            <textarea name="projects_keywords" rows="1" class="form-control no-resize auto-growth">
                                 {{ $commonSeo->projects_keywords }}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">OGG изображение страницы проектов</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($commonSeo->img('default', 'contacts_vk_image')) }}">
                        <div class="btn-group bootstrap-select form-control show-tick"  >
                            <input class="one-img-upload" type="file" name="projects_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Редактировать">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop