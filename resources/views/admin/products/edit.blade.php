@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Редактировать продукт
            </h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <a href="{{ route('product.show', ['id' => $product->id ]) }}" target="_blank" class="js-search" data-close="true">
                        <div class="demo-google-material-icon">
                            <i class="material-icons" style="float:left;margin-top:8px;">redo</i>
                            <span class="icon-name" style="float:left;margin-top:10px;">Перейти к странице новости на сайте</span>
                        </div>
                    </a>
                </div>
            </div>
            {{ Form::open(['method' => 'PATCH', 'files' => true, 'url' => route('product.update', ['id' => $product->id])]) }}
            <input name="status" type="hidden" value="1">
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Название</label>
                            <input name="name" type="text" class="form-control" value="{{ $product->name }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Старая цена</label>
                            <input name="old_cost" type="text" class="form-control" value="{{ $product->cost }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Цена</label>
                            <input name="cost" type="text" class="form-control" value="{{ $product->cost }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Краткое описание</label>
                            <textarea name="short_description" rows="1" class="form-control no-resize auto-growth"> {{ $product->short_description }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Полное описание</label>
                            <textarea id="description" name="description">{{ $product->description }}</textarea>
                            <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
                            <script>
                                CKEDITOR.replace( 'description' );
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">Категория</label>
                        <select id="category" name="category_id" class="form-control show-tick" data-live-search="true">
                            <option selected value="{{ $product->category_id }}">{{ $product->category->name }}</option>
                            @foreach($productCategories as $item)
                                @if($item->subCategories->count() == 0)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div id="attributes" class="form-group">
                        @foreach($productCategories as $item)
                            @if($item->subCategories->count() == 0)
                                <div class="attributes-wrap attributes-wrap-{{ $item->id }}" style="display:none;">
                                    @foreach($item->attributeTypes as $attributeType)
                                        <label class="form-label col-sm-12">{{ $attributeType->name}}</label>
                                        <select name="attributes[]" class="form-control show-tick">
                                            <option selected value="">— Please select —</option>
                                            @foreach($attributeType->attributes as $attribute)
                                                @php
                                                    $filtered = $product->attributes->filter(function ($value, $key) {
                                                        return $value->attribute_id == $value->id;
                                                    });   
                                                @endphp
                                                @if (!empty($filtered))
                                                    <option selected value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                                                @endif
                                                <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                                            @endforeach
                                        </select>
                                    @endforeach
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div id="wrap-racourse" class="row clearfix">
                <div class="wrap-dwnld-photo col-sm-6 col-sm-offset-3">
                    <span class="add-photo-ico racurs-margin-ico uk-icon-justify uk-icon-plus"></span>
                    <span class="add-photo-text racurs-margin-text">Добавить фотографии в слайдер</span>
                    <input id="files" class="input-dwnld-view-photo" type="file" multiple name="images[]">
                </div>
                <ul class="col-sm-6 col-sm-offset-3 uk-list angles-list uk-grid uk-grid-width-1-3 uk-margin-remove">
                    @foreach($product->commonImages as $image)
                        <li class="list-unstyled racurs-list-item col-sm-4">
                            <div class="uk-overlay uk-width-1-1">
                                <img class="img-thumbnail img-responsive" src="{{ Storage::url($image->img('default', 'img')) }}">
                            </div>
                            <a href="{{ route('common_image.destroy', ['id' => $image->id]) }}" class="col-sm-12 delete-img btn btn-danger">Удалить</a> 
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO заголовок</label>
                            <input name="seo_title" type="text" class="form-control" value="{{ $product->seo_title }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO описание</label>
                            <input name="seo_description" type="text" class="form-control" value="{{ $product->seo_description }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO ключвые слова</label>
                            <input name="seo_keywords" type="text" class="form-control" value="{{ $product->seo_keywords }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">SEO изображение</label>
                        <img class="img-responsive img-thumbnail" src="{{ Storage::url($product->seo_vk_image) }}">
                        <div class="btn-group bootstrap-select form-control show-tick">
                            <input class="one-img-upload" type="file" name="seo_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Редактировать">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop