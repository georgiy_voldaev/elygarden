@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Добавить продукт
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'POST', 'files' => true, 'url' => route('product.store')]) }}
            <input name="status" type="hidden" value="1">
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Название</label>
                            <input name="name" type="text" class="form-control" value="{{ old('name') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Старая цена</label>
                            <input name="old_cost" type="text" class="form-control" value="{{ old('old_cost') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Цена</label>
                            <input name="cost" type="text" class="form-control" value="{{ old('cost') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Краткое описание</label>
                            <textarea name="short_description" rows="1" class="form-control no-resize auto-growth">{{ old('short_description') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Полное описание</label>
                            <textarea id="description" name="description">{{ old('description') }}</textarea>
                            <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
                            <script>
                                CKEDITOR.replace( 'description' );
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">Категория</label>
                        <select id="category" name="category_id" class="form-control show-tick" data-live-search="true">
                            @foreach($productCategories as $item)
                                @if($item->subCategories->count() == 0)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div id="attributes" class="form-group">
                        @foreach($productCategories as $item)
                            @if($item->subCategories->count() == 0)
                                <div class="attributes-wrap attributes-wrap-{{ $item->id }}" style="display:none;">
                                    @foreach($item->attributeTypes as $attributeType)
                                        <label class="form-label col-sm-12">{{ $attributeType->name}}</label>
                                        <select name="attributes[]" class="form-control show-tick">
                                            <option value="">--Выберите опцию--</option>
                                            @foreach($attributeType->attributes as $attribute)
                                                <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                                            @endforeach
                                        </select>
                                    @endforeach
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div id="wrap-racourse" class="row clearfix">
                <div class="wrap-dwnld-photo col-sm-6 col-sm-offset-3">
                    <span class="add-photo-ico racurs-margin-ico uk-icon-justify uk-icon-plus"></span>
                    <span class="add-photo-text racurs-margin-text">Добавить фотографии в слайдер</span>
                    <input id="files" class="input-dwnld-view-photo" type="file" multiple name="images[]">
                </div>
                <ul class="col-sm-6 col-sm-offset-3 uk-list angles-list uk-grid uk-grid-width-1-3 uk-margin-remove"></ul>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO заголовок</label>
                            <input name="seo_title" type="text" class="form-control" value="{{ old('cost') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO описание</label>
                            <input name="seo_description" type="text" class="form-control" value="{{ old('cost') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>SEO ключевые слова</label>
                            <input name="seo_keywords" type="text" class="form-control" value="{{ old('cost') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <label class="form-label col-sm-12">SEO изображение</label>
                        <img class="img-responsive img-thumbnail" src="">
                        <div class="btn-group bootstrap-select form-control show-tick">
                            <input class="one-img-upload" type="file" name="seo_vk_image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Добавить">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop