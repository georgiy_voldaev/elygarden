@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Список атрибутов
            </h2>
            <ul class="header-dropdown m-r--5">
                <a href="{{route('attribute.create')}}" class="btn btn-primary waves-effect">Добавить атрибут</a>
            </ul>
        </div>
        <div class="body table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($attributes as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->name }}</td>
                        <td>
                            {{ Form::open(['method' => 'DELETE', 'url' => route('attribute.destroy', ['id' => $item->id])]) }}
                                <button class="col-sm-offset-3 btn btn-danger waves-effect col-sm-5" type="submit">Удалить</button>
                            {{ Form::close() }}
                            <a href="{{route('attribute.edit', ['id' => $item->id]) }}" class="col-sm-offset-3 btn btn-warning waves-effect col-sm-5">Редактировать</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $attributes->render() }}
        </div>
    </div>
@stop