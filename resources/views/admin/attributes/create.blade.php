@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Добавить атрибут
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'POST', 'url' => route('attribute.store')]) }}
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Название</label>
                            <input name="name" type="text" class="form-control" value="{{ old('name') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Тип атрибута</label>
                            <select name="type_id" class="form-control show-tick">
                                <option value="">— Please select —</option>
                                @foreach($attributeTypes as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
             <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Категория продукта, к которой относится атрибут</label>
                            <select name="type_cat_id" class="form-control show-tick">
                                <option value="">— Please select —</option>
                                @foreach($productCategories as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Добавить">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop