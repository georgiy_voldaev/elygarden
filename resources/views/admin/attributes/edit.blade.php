@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Редактировать атрибут
            </h2>
        </div>
        <div class="body">
            {{ Form::open(['method' => 'PATCH', 'url' => route('attribute.update', ['id' => $attribute->id])]) }}
            @if (count($errors) > 0)
                <div class="row clearfix">
                    <div class="col-sm-6 alert alert-danger alert-dismissable col-sm-offset-3">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        @foreach ($errors->all() as $message)
                            <div class="">
                                {{$message}}
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Название</label>
                            <input name="name" type="text" class="form-control" value="{{ $attribute->name }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <select class="form-control show-tick" name="type_id">
                        <option selected value="{{ $attribute->type->id }}">{{ $attribute->type->name }}</option>
                        @foreach($attributeTypes as $item) 
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Категория продукта, к которой относится атрибут</label>
                            <select name="type_cat_id" class="form-control show-tick">
                                <option selected value="{{ $attribute->category->id }}">{{ $attribute->category->name }}</option>
                                @foreach($productCategories as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
                <input type="submit" class="btn btn-primary col-sm-6 text-center" value="Редактировать">
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop