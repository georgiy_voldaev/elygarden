@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Просмотр вопроса
            </h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Имя</label>
                            <div>
                                {{ $problem->name }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Телефон</label>
                            <div>
                                {{ $problem->phone }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Email</label>
                            <div>
                                {{ $problem->email }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="form-group">
                        <div class="form-line">
                            <label>Сообщение</label>
                            <div>
                                {{ $problem->message }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-sm-3"></div>
            </div>
        </div>
    </div>
@stop