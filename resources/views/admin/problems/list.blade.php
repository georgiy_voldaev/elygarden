@extends('admin.layout')
@section('content')
    <div class="card">
        <div class="header">
            <h2>
                Список вопросов
            </h2>
        </div>
        <div class="body table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Имя</th>
                    <th>Телефон</th>
                    <th class="text-center">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($problems as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->phone }}</td>
                        <td>
                            <a href="{{route('problem.show', ['id' => $item->id]) }}" class="col-sm-offset-3 btn btn-warning waves-effect col-sm-5">Просмотреть</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $problems->render() }}
        </div>
    </div>
@stop