@if (Session::has('message'))
    <div class="animated fadeInDown common-alert">
        <button class="close common-close">×</button>
        <span data-notify="icon"></span>
        <span data-notify="title"></span>
        <h4 data-notify="message">{{ Session::get('message') }}</h4>
        <a href="#" target="_blank" data-notify="url"></a>
    </div>
@endif