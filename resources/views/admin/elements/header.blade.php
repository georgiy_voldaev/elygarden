<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="{{ route('user.edit', ['id' => Auth::user()->id]) }}">ELYGARDEN - админ панель</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ route('page.main') }}" target="_blank" class="js-search" data-close="true">
                        <div class="demo-google-material-icon">
                            <i class="material-icons" style="float:left;margin-top:8px;">redo</i>
                            <span class="icon-name" style="float:left;margin-top:10px;">На сайт</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>