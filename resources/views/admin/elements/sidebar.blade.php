<section>
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{route('user.logout')}}"><i class="material-icons">input</i>выход</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="menu">
            <ul class="list">
                <li class="header">Навигация</li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block toggled">
                        <i class="material-icons">event</i>
                        <span>Атрибуты</span>
                    </a>
                    <ul class="ml-menu" style="display: block;">
                        <li class="{{ route('attribute.index') === URL::current() ? 'active' : '' }}">
                            <a href="{{ route('attribute.index') }}">
                                <span>Атрибуты</span>
                            </a>
                        </li>
                        <li class="{{ route('attribute_type.index') === URL::current() ? 'active' : '' }}">
                            <a href="{{ route('attribute_type.index') }}">
                                <span>Типы атрибутов</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block toggled">
                        <i class="material-icons">card_travel</i>
                        <span>Продукты</span>
                    </a>
                    <ul class="ml-menu" style="display: block;">
                        <li class="{{ route('product.listing') === URL::current() ? 'active' : '' }}">
                            <a href="{{ route('product.listing') }}">
                                <span>Продукты</span>
                            </a>
                        </li>
                        <li class="{{ route('product_category.index') === URL::current() ? 'active' : '' }}">
                            <a href="{{ route('product_category.index') }}">
                                <span>Категории продуктов</span>
                            </a>
                        </li>
                    </ul>
                </li> 
                <li class="{{ route('project.listing') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('project.listing') }}">
                        <i class="material-icons">create</i>
                        <span>Проекты</span>
                    </a>
                </li> 
                <li class="{{ route('service.listing') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('service.listing') }}">
                        <i class="material-icons">create</i>
                        <span>Услуги</span>
                    </a>
                </li>
                <li class="{{ route('banner.index') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('banner.index') }}">
                        <i class="material-icons">create</i>
                        <span>Баннеры</span>
                    </a>
                </li>
                <li class="{{ route('license.index') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('license.index') }}">
                        <i class="material-icons">create</i>
                        <span>Лицензии</span>
                    </a>
                </li>
                <li class="{{ route('worker.index') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('worker.index') }}">
                        <i class="material-icons">create</i>
                        <span>Работники</span>
                    </a>
                </li>              
                <li class="{{ route('problem.index') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('problem.index') }}">
                        <i class="material-icons">live_help</i>
                        <span>Вопросы</span>
                    </a>
                </li>
                <li class="{{ route('user.index') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('user.index') }}">
                        <i class="material-icons">people_outline</i>
                        <span>Пользователи</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block toggled">
                        <i class="material-icons">settings</i>
                        <span>Общие настройки</span>
                    </a>
                    <ul class="ml-menu" style="display: block;">
                        <li class="{{ route('common_seo.edit', ['id' => 1]) === URL::current() ? 'active' : '' }}">
                            <a href="{{ route('common_seo.edit', ['id' => 1]) }}">
                                <span>Общее SEO</span>
                            </a>
                        </li>
                        <li class="{{ route('setting.edit', ['id' => 1]) === URL::current() ? 'active' : '' }}">
                            <a href="{{ route('setting.edit', ['id' => 1]) }}">
                                <span>Контактная информация</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </aside>
</section>
