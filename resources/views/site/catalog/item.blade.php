@extends('site.layout', 
    [
        'title' => $commonSeo->seo_title,   
        'description' => $commonSeo->seo_description,   
        'keywords' => $commonSeo->seo_keywords,   
        'og_image' => url('/').Storage::url($commonSeo->seo_og_image),   
        'vk_image' => url('/').Storage::url($commonSeo->seo_vk_image),   
    ]
)
@section('content')
    <main class="margin-catalog">
        @include('site.elements.subnav', 
            [
                'current' => ' <li><a href="'.route('product.index').'">Каталог</a></li><li><span>'.$product->name.'</span></li>'
            ]
        )
        <div class="uk-width-1-1 main-wrap">
            <div class="uk-width-2-3 uk-container-center common-wrap">
                <div class="uk-width-1-1 uk-grid uk-grid-collapse">
                    @include('site.catalog.elements.product.slider')
                    @include('site.catalog.elements.product.main_info')
                </div>
                <div class="uk-width-1-1 tab-wrap">
                    @include('site.catalog.elements.product.tabs.tabs')
                </div>
                @include('site.catalog.elements.product.similar_products')
            </div>
        </div>
    </main>
@endsection