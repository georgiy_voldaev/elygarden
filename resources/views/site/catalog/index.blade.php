@extends('site.layout', 
    [
        'title' => $commonSeo->catalog_title,   
        'description' => $commonSeo->catalog_description,   
        'keywords' => $commonSeo->catalog_keywords,
        'ogg_image' => '', 
        'og_title' => '', 
        'og_description' => $commonSeo->catalog_description,
    ]
)
@section('content')
    <main class="margin-catalog">
        @include('site.catalog.elements.common_subnav', ['current' => '<li><span>Каталог</span></li>'])
        <div class="main">
            <div class="uk-width-1-1">
                <div class="uk-width-2-3 uk-grid uk-grid-collapse uk-container-center list-products-wrap">
                    <h1 class="title uk-width-1-1 uk-hidden">
                        Каталог
                    </h1>
                    @php
                        /**
                         * Поиск родительской категории и подготовка данных 
                         * для вывода их в кнопке назад в каталоге.
                         * Используется в sidebar.view, desktop_hidden_props.view
                         **/
                        $catalogLink = '';
                        $backText = 'Каталог';
                        if (!empty($category->parent)) {
                            $catalogLink = '?category='.$category->parent->id;
                            $backText = $category->parent->name;
                        }
                    
                        /**
                         * Подготовка переменной clearAttribute. 
                         * Эта переменная нужна для компоновки верного меню опций товара.
                         */
                        if (isset($_GET['attribute'])) {
                            $i = 0;
                            $and = '';
                            $clearAttribute = urldecode(Request::fullUrl());
                            foreach ($_GET['attribute'] as $attribute) {
                                $i == 0 ? $and = '' : $and = '&'; 
                                $clearAttribute = preg_replace( '~'.$and.'attribute\['.$i.'\]='.$attribute.'~', '' , $clearAttribute);
                                $i += 1;
                            }
                        } else {
                            $clearAttribute = urldecode(Request::fullUrl());
                        }   
                    @endphp
                    @include('site.catalog.elements.products.sidebar')
                    <div class="uk-width-large-8-10 uk-width-medium-1-1 products-wrap">
                        @include('site.catalog.elements.products.desktop_hidden_props')
                        @include('site.catalog.elements.products.sort_panel')
                        <p class="description-product-item uk-width-1-1" style="padding:10px;">Внимание! Цены уточняйте. Минимальный заказ от 10 000 рублей.</p>

                        <div class="uk-width-1-1 uk-grid uk-grid-collapse">
                            @if ($view == 'card')
                                @foreach ($products as $item)
                                    @include('site.catalog.elements.products.item_card')                          
                                @endforeach
                            @else
                                @foreach ($products as $item)
                                    @include('site.catalog.elements.products.item_list')                                
                                @endforeach   
                            @endif
                        </div
                          <?php
                          	$attributes = [];
                          	if (isset($_GET['category'])) {
                                $attributes += ['category' => $_GET['category']];
                            }
                          	if (isset($_GET['attribute'])) {
                            	$attributes += ['attribute' => $_GET['attribute']];
                            }
                          	if (isset($_GET['min_cost'])) {
                            	$attributes += ['min_cost' => $_GET['min_cost']];
                            }
                            if (isset($_GET['max_cost'])) {
                            	$attributes += ['max_cost' => $_GET['max_cost']];
                            }
                          ?>
                          {{ $products->appends($attributes)->render() }}
                    </div>
                </div>    
            </div>
        </div>
    </main>
@endsection