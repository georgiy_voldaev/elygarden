@extends('site.layout', 
    [
        'title' => $commonSeo->catalog_title,   
        'description' => $commonSeo->catalog_description,   
        'keywords' => $commonSeo->catalog_keywords,   
        'og_image' => url('/').Storage::url($commonSeo->catalog_ogg_vk),   
        'vk_image' => url('/').Storage::url($commonSeo->catalog_ogg_all),   
    ]
)
@section('content')
    <header data-parallax="scroll" data-z-index="0" data-speed="0.5" data-image-src="/img/covers/catalog.jpg" class="parallax-window uk-margin-large-top uk-overlay top-block common-header">
        <div class="uk-overlay-panel overlay-about uk-vertical-align">
            <div class="uk-vertical-align-middle uk-width-1-1">
                <div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1 uk-container-center">
                    <h1 class="part-site-title">Каталог</h1>
                </div>
            </div>
        </div>
    </header>
    <main class="margin-catalog uk-margin-remove">
        @include('site.catalog.elements.common_subnav')
        <div class="main">
            <div class="uk-width-1-1">
                <div class="uk-width-2-3 uk-grid uk-grid-collapse uk-container-center list-products-wrap">
                    <h1 class="title uk-width-1-1 uk-hidden">
                        Каталог
                    </h1>
                    <div class="uk-width-1-1 products-wrap">
                        <p class="description-product-item">Внимание! Цены уточняйте. Минимальный заказ от 10 000 рублей.</p>
                        <div class="uk-width-1-1 uk-grid uk-grid-collapse uk-grid-width-small-1-1 uk-grid-width-medium-1-2 uk-grid-width-large-1-4">
                            @foreach ($categories as $item)
                                @include('site.catalog.elements.products.category')
                            @endforeach
                        </div
                    </div>
                </Grid>    
            </div>
        </div>
    </main>
@endsection