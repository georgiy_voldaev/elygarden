<div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1 uk-hidden-small main-info-wrap">
    <span class="bar-code">
        <b>Артикул:</b>
        {{ $product->barcode }}
    </span>
    <h1 class="title-prod">{{ $product->name }}</h1>
    <p class="description-product-item">Внимание! Цены уточняйте. Минимальный заказ от 10 000 рублей.</p>
    <p class="description-product-item">{!! $product->description !!}</p>
    {{--<div class="cost"> От {{ $product->cost }} <i class="uk-icon-justify uk-icon-ruble"></i></div>--}}
</div>