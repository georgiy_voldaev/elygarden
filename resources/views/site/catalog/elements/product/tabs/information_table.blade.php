<table class="uk-table uk-table-striped">
    <tbody>
        @foreach($attributes as $item)
            <tr>
                <td>
                    <strong style="color:black;font-weight:500;">{{ $item->type->name }}</strong>
                </td>
                <td>
                    {{ $item->name }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>