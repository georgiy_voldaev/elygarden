<ul class="uk-tab" data-uk-tab="{connect:'#my-id'}">
    <li class="uk-active"><a href="">Полное описание</a></li>
    <li><a href="">Характеристики</a></li>
</ul>

<ul id="my-id" class="uk-switcher uk-margin">
    <li class="uk-active">
        @include('site.catalog.elements.product.tabs.details')                
    </li>
    <li class="">
        @include('site.catalog.elements.product.tabs.information_table')   
    </li>
</ul>