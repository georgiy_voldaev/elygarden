<div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
    <div class="uk-hidden-large uk-hidden-medium">
        <span class="bar-code">
            <b>Артикул:</b>
            {{ $product->barcode }}
        </span>
        <h1 class="title-prod">{{ $product->name }}</h1>
        <p class="description-product-item">{{ $product->description }}</p>
        <div class="cost">{{ $product->cost }} <i class="uk-icon-justify uk-icon-ruble"></i></div>
    </div>
    <div class="owl-carousel product-slider">
        @foreach ($product->commonImages as $item)
            <div data-hash="{{ $loop->iteration }}">
                <img src="{{ Storage::url($item->img('slide', 'img')) }}" alt="">
            </div>    
        @endforeach
    </div>
    @foreach ($product->commonImages as $item)
        <a class="dots-prod" href="#{{ $loop->iteration }}">
            <img src="{{ Storage::url($item->img('dot', 'img')) }}" alt="">
        </a>
    @endforeach
</div>