<div class="uk-width-1-1 similar-products-wrap uk-grid uk-grid-collapse" uk-grid>
    <h2 class="product-similar-title uk-width-1-1">
        Похожие товары
    </h2>
    @foreach ($productRecommended as $item)
        <div class="uk-width-large-1-4 uk-width-small-1-1 uk-width-medium-1-2 card-wrap">
            <a href="{{ route('product.show', ['id$' => $item->id]) }}" class="uk-card uk-card-default uk-card-hover uk-card-body uk-light card">
                <div class="uk-card-media-top">
                    <img class="media" src="{{ Storage::url($item->commonImages->first()->img('default', 'img')) }}" alt="">
                </div>
                <div class="uk-card-body">
                    <h3 class="title-card uk-card-title">
                        {{ $item->name }}
                    </h3>
                    <div class="price">
                        {{ $item->cost }} <i class="uk-icon-justify uk-icon-ruble"></i>
                    </div>
                </div>
            </a>    
        </div>
    @endforeach
</div>