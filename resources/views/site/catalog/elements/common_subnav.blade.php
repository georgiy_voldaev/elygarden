<div class="breadcrumb">
    <ul class="uk-breadcrumb uk-width-2-3 uk-container-center">
        <li>
            <a href="{{ route('page.main') }}">
                Главная
            </a>
        </li>
        <li>
            <a href="{{ route('product.index')}}">
                Каталог
            </a>
        </li>
        @if (isset($category))
            @include('site.catalog.elements.el_subnav')
        @endif
    </ul>            
</div>