 <div class="uk-accordion sidebar-wrap choose-options {{ $visibility }}" data-uk-accordion="">
    <h4 class="uk-accordion-title">Выбранные опции</h4>
    <div class="uk-accordion-content">
        <ul class="uk-list">
            @if (!empty($activeAttributes))
                @foreach ($activeAttributes as $attribute)
                    <li>
                        @if (isset($_GET['attribute']))
                            @php
                                $currentAttributes = '';
                                foreach ($activeAttributes as $item) {
                                    $item->id == $attribute->id ? $currentAttributes .= '' : $currentAttributes .= '&attribute[]='.$item->id;
                                }
                            @endphp 
                        @endif
                        <a class="props-item" href="{{ $clearAttribute.$currentAttributes }}">
                            Атрибут: {{ $attribute->name }}                            
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
    <a href="/products?{{ 'category='.$_GET['category'] }}" class="clear-btn">
        Очистить всё
    </a>
</div>