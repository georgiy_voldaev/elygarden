@if (isset($_GET['attribute']))
    @include('site.catalog.elements.products.choose_props', ['visibility' => 'uk-visible-large'])
@endif
<h3 class="uk-width-1-1 product-sidebar-title uk-visible-large">Опции</h3>
<div class="uk-accordion sidebar-wrap" data-uk-accordion>
    @include('site.catalog.elements.products.price_sort_panel')
    @foreach ($attributeTypes as $item)
        <h4 class="uk-accordion-title">{{ $item->name }}</h4>
        <div class="uk-accordion-content">
            <ul class="uk-list">
                @foreach ($item->attributes()->categoryAttributes($category->id)->get() as $attribute)
                    <li>
                        @php
                            /**
                             * Поиск активных опций, сравнивая текущую опцию со всеми.
                             */
                            $active = '';
                            $currentAttribute = '';
                        
                            if (!empty($activeAttributes)) {
                                $currentAttributes = '';
                                foreach ($activeAttributes as $item) {
                                    $currentAttribute = '';
                                    if ($item->id == $attribute->id){
                                        $active = 'active';
                                        $currentAttributes .= '';
                                    } else {
                                        $currentAttribute = '&attribute[]='.$attribute->id;
                                        $currentAttributes .= '&attribute[]='.$item->id;
                                    }
                                }

                                foreach ($activeAttributes as $item) {
                                    if ($item->id == $attribute->id){
                                        $currentAttribute = '';
                                    }
                                }
                            } else { 
                                $currentAttributes = '&attribute[]='.$attribute->id;
                            }
                        @endphp
                        <a class="props-item {{ $active }}" href="{{ $clearAttribute.$currentAttributes.$currentAttribute }}">
                            {{ $attribute->name }}                            
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endforeach
</div>