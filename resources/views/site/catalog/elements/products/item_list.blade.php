<div class="uk-width-1-1 list-item-wrap">
    <a class="uk-width-1-1 list-item-container" href="{{ route('product.show', ['id$' => $item->id]) }}">
        <div class="wrap-media">
            <img src="{{ Storage::url($item->commonImages->first()->img('catalog_preview', 'img')) }}" alt="">
        </div>
        <div class="title-wrap">
            <h3 class="title-card">
                {{ $item->name }}
            </h3>
        </div>
        {{--<div class="wrap-price">--}}
            {{--<div class="price">--}}
                {{--От {{ $item->cost }} <i class="uk-icon-justify uk-icon-ruble"></i>--}}
            {{--</div>--}}
        {{--</div>--}}
    </a>
</div>
