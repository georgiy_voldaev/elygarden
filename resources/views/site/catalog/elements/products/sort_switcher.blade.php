@php
    $clearSort = preg_replace('~&sort=decrease~' , '' , preg_replace( '~&sort=increase~' , '' , urldecode(Request::fullUrl())));
@endphp
<select onInput="window.location.href = this.value" class="wrap-sort__panel__item__name__sort-switcher" id="form-h-s">
    <option {{ $clearSort == Request::fullUrl() ? 'selected' : '' }}
            value="{{ $clearSort }}">По умолчанию</option>
    <option {{ strpos(Request::fullUrl(), '&sort=increase') != false ? 'selected' : '' }} 
            value="{{ $clearSort.'&sort=increase' }}">По возрастанию цены</option>
    <option {{ strpos(Request::fullUrl(), '&sort=decrease') != false ? 'selected' : '' }}
            value="{{ $clearSort.'&sort=decrease' }}">По убыванию цены</option>
</select>