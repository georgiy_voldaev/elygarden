<div class="uk-width-1-1 uk-grid uk-grid-collapse uk-hidden-large">
    @if (isset($_GET['attribute']))
        @include('site.catalog.elements.products.choose_props', ['visibility' => 'uk-hidden-large'])
    @endif
    <div class="uk-width-1-2 back-btn-mob-wrap">
        <a class="btn-back-catalog" href="{{ route('product.index').$catalogLink }}">
            <span class="btn-back-icon uk-icon-angle-left"></span>
            <span class="btn-back-text">
                {{ $backText }}
            </span>
        </a>
    </div>
    <div class="uk-width-1-2 fiter-btn-mob-wrap">
        <a href="#filter" data-uk-offcanvas="{mode:'slide'}" class="btn-back-catalog">
            <span class="btn-back-text">
                Фильтры
            </span>
            <span class="btn-back-icon uk-icon-sliders"></span> 
        </a>
        <div id="filter" class="uk-offcanvas mob-filter-menu">
            <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
                <div class="uk-icon-close filter-close"></div>
                @include('site.catalog.elements.products.props_filter')  
            </div>
        </div>
    </div>
</div>