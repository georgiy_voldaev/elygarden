@php
    $clearView = preg_replace('~&view=list~' , '' , preg_replace( '~&view=card~' , '' , urldecode(Request::fullUrl())));
@endphp
<div class="uk-width-1-1 wrap-sort__panel__item__name" style="width:100%;">
    <a href="{{ $clearView.'&view=card' }}" 
       class="uk-icon-justify uk-icon-th wrap-sort__panel__item__name__ico
       {{ strpos(Request::fullUrl(), '&view=card') != false ? 'active' : '' }}">
    </a>
    <a href="{{ $clearView.'&view=list' }}" 
        class="uk-icon-justify uk-icon-th-list wrap-sort__panel__item__name__ico
        {{ strpos(Request::fullUrl(), '&view=list') != false ? 'active' : '' }}">
    </a>
</div>