<div class="uk-width-2-10 uk-visible-large">
    <a class="btn-back-catalog" href="{{ route('product.index').$catalogLink }}">
        <span class="btn-back-icon uk-icon-angle-left"></span>
        <span class="btn-back-text">
            {{ $backText }}
        </span>
    </a>
    @include('site.catalog.elements.products.props_filter')
</div>