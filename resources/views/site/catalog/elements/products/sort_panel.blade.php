<div class="uk-width-1-1 wrap-sort">
    <div class="uk-width-1-1 uk-grid uk-grid-collapse wrap-sort__panel">
        <div class="uk-width-large-1-3 uk-width-small-1-2 uk-width-medium-1-2 uk-grid uk-grid-collapse wrap-sort__panel__item">
            <div class="uk-width-1-2 uk-hidden-small wrap-sort__panel__item__name">
                Сортировка:
            </div>
            <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
                @include('site.catalog.elements.products.sort_switcher')
            </div>
        </div>
        <div class="uk-width-large-1-3 uk-width-small-1-2 uk-width-medium-1-4 wrap-sort__panel__item wrap-sort__panel__item--center">
            <div class="wrap-sort__panel__item__name">
                Всего<span class="uk-hidden-small"> товаров</span>: {{ $numProducts }}
            </div>
        </div>
        <div class="uk-width-large-1-3 uk-grid uk-hidden-small uk-width-medium-1-4 uk-text-right uk-grid-collapse wrap-sort__panel__item">
            @include('site.catalog.elements.products.view_switcher')
        </div>
    </div>
</div>