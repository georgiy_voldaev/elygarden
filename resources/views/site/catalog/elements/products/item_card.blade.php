<div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-1 card-wrap">
    <a href="{{ route('product.show', ['id$' => $item->id]) }}" class="uk-card uk-card-default uk-card-hover uk-card-body uk-light card">
        <div class="uk-card-media-top">
            <img class="media" src="{{ Storage::url($item->commonImages->first()->img('catalog_preview', 'img')) }}" alt="">
        </div>
        <div class="uk-card-body">
            <h3 class="title-card uk-card-title">
                {{ $item->name }}
            </h3>
            {{--<div class="price">--}}
                {{--От {{ $item->cost }} <i class="uk-icon-justify uk-icon-ruble"></i>--}}
            {{--</div>--}}
        </div>
    </a>    
</div>