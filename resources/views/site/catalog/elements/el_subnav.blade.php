@if ($category->parent_id != 0)
    @include('site.catalog.elements.el_subnav', ['category' => $category->parent])
@endif
<li>
    @if ($category->id == $_GET['category'])
        <span>
            {{  $category->name }}
        </span>
    @else
        <a href="{{ route('product.index').'?category='.$category->id }}">
            {{  $category->name }}
        </a>
    @endif
</li>