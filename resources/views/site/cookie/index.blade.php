@extends('site.layout', 
    [
        'title' => 'Садово парковая компания "Элизиум" - уведомление о куки',   
        'description' => '',   
        'keywords' => '',   
        'og_image' => url('/').Storage::url($commonSeo->contacts_ogg_vk),   
        'vk_image' => url('/').Storage::url($commonSeo->contacts_ogg_all),   
    ]
)
@section('content')
    <main>
        <header data-parallax="scroll" data-z-index="0" data-speed="0.5" data-image-src="/img/covers/1.jpg" class="parallax-window uk-margin-large-top uk-overlay top-block common-header">
            <div class="uk-overlay-panel overlay-about uk-vertical-align">
                <div class="uk-vertical-align-middle uk-width-1-1">
                    <div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1 uk-container-center">
                        <h1 class="part-site-title">Уведомление о файлах cookie</h1>
                    </div>
                </div>
            </div>
        </header>
        <section class="uk-width-2-3 uk-container-center">
          <h3 style="margin-top: 15px;">Компания Элизиум.</h3>
          <p style="margin-top: 15px;">
			   <b>ИП</b> Макаров Антон Анатольевич<br>
               <b>ИНН</b> 602720734904<br>
 			   <b>Юридический адрес</b> Псков, Луковский проезд, д1 180011<br>
          </p>
          <p>
               Компания "Элизиум" стремится к тому, чтобы взаимодействие с нашим веб-сайтом было максимально информативным и отвечало вашим интересам. Для этого мы используем файлы cookie и подобные средства. Мы считаем, что вам 				   	   важно знать, какие файлы cookie использует наш веб-сайт и для каких целей. Это поможет защитить ваши персональные сведения и обеспечит максимальное удобство нашего веб-сайта для пользователя.
          </p>

          <h3>Что представляют из себя cookie-файлы?</h3>
          <p>
			   Файлы cookie представляют собой небольшие текстовые файлы, которые сохраняются на вашем компьютере или мобильном устройстве при посещении определенных веб-сайтов.<br>
               Компанией "Элизиум" могут использоваться подобные технологии, например пиксели, веб-маяки, "отпечатки" устройства и т. д. В дальнейшем все эти технологии будут именоваться собирательно "файлы cookie".
          </p>
          
          <h3>Для чего используются файлы cookie?</h3>
          <p>
               Файлы cookie могут использоваться в различных целях. Во-первых, они могут требоваться для нормальной работы веб-сайта.<br>
               Например, без файлов cookie веб-сайт не сможет запомнить, что вы выполнили вход или поместили товары в корзину. Такие файлы cookie называются строго необходимыми.
          </p>
          <p>
               Файлы cookie также нужны для анализа использования веб-сайта, подсчета количества посетителей и усовершенствования веб-сайта. <br>
               Мы не ассоциируем статистику использования веб-сайта и другие отчеты с конкретными людьми. Такие файлы cookie называются аналитическими.
          </p>
          <p>
               В третьих,файлы cookie для социальных сетей используются для интеграции социальных сетей с веб-сайтом, чтобы вы могли использовать функции "Нравится" и "Поделиться" в любимой социальной сети.
          </p>
          <p>
               Четвертая важная сфера применения файлов cookie — онлайн-реклама. С их помощью на веб-сайте "Элизиум" и других веб-сайтах отображаются только наиболее полезные и интересные для вас объявления. 
               Такие файлы cookie называются рекламными.
          </p>
          
          <h3>Как управлять файлами cookie или отключать их в браузере?</h3>
          <p>
               Вы можете найти информацию о том, как отключить файлы cookie или изменить настройки файлов cookie для браузера, перейдя по следующим ссылкам:<br>
               <b>Google Chrome:</b> <a href="https://support.google.com/chrome/answer/95647?hl=en">https://support.google.com/chrome/answer/95647?hl=en</a><br>
			   <b>Firefox:</b> <a href="https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences">https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences</a><br>
			   <b>Internet Explorer:</b> <a href="http://windows.microsoft.com/en-GB/windows-vista/Block-or-allow-cookies">http://windows.microsoft.com/en-GB/windows-vista/Block-or-allow-cookies</a><br>
			   <b>Safari:</b> <a href="http://help.apple.com/safari/mac/8.0/#/sfri11471">http://help.apple.com/safari/mac/8.0/#/sfri11471</a>
          </p>
        </section>
    </main>
@endsection