<section class="uk-block about-section uk-panel-box stat-block">
    <div class="uk-grid uk-width-2-3 uk-container-center">
        <a class="ico-wrap uk-width-large-1-4 uk-width-small-1-1 uk-width-medium-1-2">
            <div class="main-icon-stat">
                <img src="{{ asset('img/icons/objects.svg') }}" alt="">
            </div>
            <h4 class="uk-text-center uk-text-uppercase main-stat-text">Выполнено более 30 объектов</h4>
        </a>
        <a class="ico-wrap uk-width-large-1-4 uk-width-small-1-1 uk-width-medium-1-2">
            <div class="main-icon-stat">
                <img src="{{ asset('img/icons/experience.svg') }}" alt="">
            </div>
            <h4 class="uk-text-center uk-text-uppercase main-stat-text">7 лет опыта озеленения</h4>
        </a>
        <a class="ico-wrap uk-width-large-1-4 uk-width-small-1-1 uk-width-medium-1-2">
            <div class="main-icon-stat">
                <img src="{{ asset('img/icons/personal.svg') }}" alt="">
            </div>
            <h4 class="uk-text-center uk-text-uppercase main-stat-text">Штат сотрудников из 15 профессиональных специалистов</h4>
        </a>
        <a class="ico-wrap uk-width-large-1-4 uk-width-small-1-1 uk-width-medium-1-2">
            <div class="main-icon-stat">
                <img src="{{ asset('img/icons/sertificate.svg') }}" alt="">            
            </div>
            <h4 class="uk-text-center uk-text-uppercase main-stat-text">Наличие сертификатов</h4>
        </a>
    </div>
</section>