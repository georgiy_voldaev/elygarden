<section class="uk-block top-block owl-carousel uk-padding-remove main-slider">
    @foreach ($banners as $banner)
        <div class="uk-overlay">
            <img class="change-img" 
                 data-default="{{ Storage::url($banner->img('default', 'img')) }}" 
                 data-middle="{{Storage::url($banner->img('middle', 'img')) }}" 
                 data-mini="{{Storage::url($banner->img('min', 'img')) }}" 
                 src="{{ Storage::url($banner->img('default', 'img')) }}">
                <figcaption class="uk-overlay-panel uk-width-large-2-3 uk-width-medium-2-3 uk-width-small-5-6 uk-container-center uk-vertical-align">
                    <div class="uk-vertical-align-middle">
                    {{--<h1 class="uk-width-1-1 slider-title">--}}
                        {{--{{ $banner->title }}--}}
                    {{--</h1>--}}
                </div>
            </figcaption>
        </div>
    @endforeach
</section>