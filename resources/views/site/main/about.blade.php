<section class="uk-block uk-overlay uk-padding-remove section-main-about">
    <img class="change-img"
         data-default="/img/covers/about-large.jpg"
         data-middle="/img/covers/about-middle.jpg"
         data-mini="/img/covers/about-mini.jpg"
         src="/img/covers/about-large.jpg"
         alt="">
    <div class="uk-overlay-panel overlay-about uk-vertical-align">
        <div class="uk-vertical-align-middle uk-width-1-1">
            <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1 uk-container-center">
                <h2 class="about-title-main uk-text-center uk-text-uppercase">О компании</h2>
                <div class="uk-text-center about-text-main-b">
                    <div data-uk-scrollspy="{cls:'uk-animation-fade'}" class="uk-width-large-2-3 uk-width-medium-2-3 uk-width-small-1-1 uk-container-center uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-fade">
                    </div>
                    <a href="{{ route('page.about') }}" class="btn-white btn-inverse uk-button-primary direction-btn uk-button about-btn">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
</section>