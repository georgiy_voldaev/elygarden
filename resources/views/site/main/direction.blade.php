<section class="uk-block uk-padding-bottom-remove">
    <h2 class="title-screen uk-text-center uk-margin-large-top">Направление деятельности</h2>
    <div class="uk-grid uk-grid-collapse uk-width-1-1 uk-container-center wrap-direction-main">
        @foreach($services as $service)
            <div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-1 direction-block">
                <div class="uk-panel uk-overlay">
                    <img src="{{ Storage::url($service->img('min', 'img'))}}" alt="">
                    <div class="uk-overlay-panel direction-hover">
                        <h3 class="uk-panel-title direction-hover-title">
                            {{ $service->name }}
                        </h3>
                        <a href="{{ route('service.show', ['id' => $service->slug]) }}" class="btn-inverse uk-button uk-button-primary direction-btn btn-white">Подробнее</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>