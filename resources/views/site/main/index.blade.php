@extends('site.layout', 
	[
		'title' => $commonSeo->main_title, 
		'og_title' => $commonSeo->main_title, 
		'ogg_image' => '',  
		'og_description' => $commonSeo->main_description, 
		'description' => $commonSeo->main_description, 
		'keywords' => $commonSeo->main_keywords
	]
)
@section('content')
    @include('site.main.banner')
    @include('site.main.direction')
    @include('site.main.advantages')
    @include('site.main.about')
@endsection