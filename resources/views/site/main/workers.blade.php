<section class="uk-block uk-text-center">
    <h2 class="title-screen">Наша команда</h2>
    <div class="owl-carousel wrap-reviews news-main-grid uk-container-center">
        @foreach ($workers as $item)
            <div class="wrap-card-review">
                <div class="card-review">
                    <img src="{{ Storage::url($item->img('default', 'img')) }}">
                    <h4 class="review-name">
                        {{ $item->name }}
                    </h4>
                    <p class="review->text">
                        {{ $item->text }}
                    </p>
                </div>
            </div>
        @endforeach
    </div>
</section>