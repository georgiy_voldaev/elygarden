<footer class="uk-block uk-grid">
    <div class="uk-grid uk-grid-collapse uk-width-2-3 uk-grid-width-small-1-1 uk-grid-width-medium-1-1 uk-grid-width-large-1-3 uk-container-center uk-padding-remove">
        <div class="footer-column uk-width-1-1 text-center">
            <div class="uk-container-center uk-text-center">
                <h4>Элизиум</h4>
                <div>    
                    <a target="_blank" class="social-link uk-icon-instagram" href="https://www.instagram.com/ely.garden/"></a>
                    <a target="_blank" class="social-link uk-icon-send" href="https://tele.click/makarovofficialrus"></a>
                    <a target="_blank" class="social-link uk-icon-whatsapp" href="https://api.whatsapp.com/send?phone=79602222525"></a>
                    <p>
                        +7 (960) 222 25 25<br>
                        <a href="mailto:info@elygarden.ru">info@elygarden.ru</a>
                        <br>
                        <a href="/policy">
                            Политика конфиденциальности
                        </a>
                    </p>
                    <p>
                        Разработка сайта
                        <a href="https://vk.com/georgiy_voldaev" target="_blank">
                          Георгий Волдаев  
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
