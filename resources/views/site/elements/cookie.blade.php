<style>
  	.wrap-cookie {
  		display: block;
        opacity: 1;
        bottom: 0;
        -webkit-transition-delay: .3s;
        transition-delay: .3s;
      	box-sizing: border-box;
    	position: fixed;
    	left: 0;
    	right: 0;
    	padding: 0 25px;
    	min-width: 320px;
    	background: #fff;
    	-webkit-box-shadow: 0 0 20px rgba(0,0,0,0.3);
    	box-shadow: 0 0 20px rgba(0,0,0,0.3);
    	z-index: 9999900;
    	-webkit-transition: bottom .7s, opacity .8s;
    	transition: bottom .7s, opacity .8s;
  	}
  
	.wrap-cookie__msg {
  		margin: 0 auto;
        padding: 29px 0 30px;
        border-top: 1px solid #eeeeee;
        max-width: 1204px;
        font-size: 12px;
        font-style: normal;
        color: #636363;
        position: relative;
  	}
	
  	.cookie-notice__desc {
  		font-size: 12px;
    	color: #636363;
    	font-size: 14px;
    	word-break: break-word;
    	-webkit-box-sizing: border-box;
    	box-sizing: border-box;
    	margin: 0;
    	padding: 0;
  	}
  
	.cookie-notice__link {
  		font-size: 12px;
	    color: #1428a0;
    	text-decoration: underline;
    	display: inline-block;
  	}
  	
	.cookie__close {
  		position: absolute;
      	left: auto;
      	right: -15px;
      	padding: 10px;
      	font-size: 0;
      	cursor: pointer;
        top: 19px;
    	border: none;
        color: black;
    	font-size: 20px;
    	background: none;
  		transition: all 0.3s ease;
  	}
  	
  	.cookie__close:hover {
        color: #059;
  	}
</style>
<!-- Cookie Notice -->
<div class="wrap-cookie" role="status">			
	<div class="wrap-cookie__msg">
		<p class="cookie-notice__desc">На этом сайте используются файлы cookie. Продолжая просмотр сайта, вы разрешаете их использование.
		<a href="{{ route('page.cookie') }}" class="cookie-notice__link">Подробнее</a></p>
		<button type="button" class="cookie__close js-close uk-icon-close"></button>
	</div>
</div>
<!-- Cookie Notice -->