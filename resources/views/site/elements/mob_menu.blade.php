    <div class="mobile-nav" mode="slide">
        <a class="mob-menu-close uk-close uk-hidden-large"></a>
        <div class="uk-width-2-3 uk-container-center menu-items-center">
            <ul class="uk-list">
                <li>
                    <a href="{{ route('page.main') }}">Главная</a>
                </li>
                <li>
                    <a href="{{ route('project.index') }}">Проекты</a>
                </li>
                <li>
                    <a href="{{ route('service.index') }}">Услуги</a>
                </li>
                <li>
                    <a href="{{ route('product.index') }}">Каталог</a>
                </li>
                <li>
                    <a href="{{ route('page.about') }}">О компании</a>
                </li>
                <li>
                    <a href="{{ route('page.contacts') }}">Контакты</a>
                </li>
            </ul>
        </div>
    </div>
