<nav class="uk-navbar main-nav uk-grid scroll-height">
    <ul class="uk-navbar-nav uk-container-center uk-width-large-2-3 uk-width-medium-5-6 list-menu">
        <li class="nav-item logo">
            <a class="uk-navbar-brand" href="{{ route('page.main') }}">
                <img class="uk-responsive-height uk-visible-large" src="/img/logo.png" alt="">
                <img class="uk-responsive-height uk-hidden-large" src="/img/logo_mini.png" alt="">
            </a>
        </li>
        <div class="uk-navbar-flip">
            <ul class="uk-navbar-nav uk-hidden-small uk-hidden-medium">
                <li class="nav-item {{ route('project.index') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('project.index') }}">Проекты</a>
                </li>
                <li class="nav-item {{ route('service.index') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('service.index') }}">Услуги</a>
                </li>
                <li class="nav-item {{ route('product.index') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('product.index') }}">Каталог</a>
                </li>
                <li class="nav-item {{ route('page.about') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('page.about') }}">О компании</a>
                </li>
                <li class="nav-item {{ route('page.contacts') === URL::current() ? 'active' : '' }}">
                    <a href="{{ route('page.contacts') }}">Контакты</a>
                </li>
                <li class="b-contact">
                    <div class="header-contact-link">
                        <a class="link-first" href="telto:+79602222525">
                            8 960 222 25 25
                        </a>
                    </div>
                </li>
            </ul>
            <a class="btn-open-mob-menu uk-navbar-toggle uk-hidden-large"></a>
        </div>
    </ul>
</nav>
