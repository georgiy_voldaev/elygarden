<div class="breadcrumb">
    <ul class="uk-breadcrumb uk-width-2-3 uk-container-center">
        <li><a href="{{ route('page.main') }}">Главная</a></li>
        {!! $current !!}
    </ul>            
</div>