@extends('site.layout', 
    [
        'title' => $commonSeo->services_title, 
        'ogg_image' => '',  
        'og_title' => $commonSeo->services_title, 
        'og_description' => $commonSeo->services_description, 
        'description' => $commonSeo->services_description, 
        'keywords' => $commonSeo->services_keywords
    ]
)
@section('content')
    <header data-parallax="scroll" data-z-index="0" data-speed="0.5" data-image-src="/img/covers/services.png" class="parallax-window uk-margin-large-top uk-overlay top-block common-header">
        <div class="uk-overlay-panel overlay-about uk-vertical-align">
            <div class="uk-vertical-align-middle uk-width-1-1">
                <div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1 uk-container-center">
                    <h1 class="part-site-title">Услуги</h1>
                </div>
            </div>
        </div>
    </header>
    <section class="uk-block uk-text-center uk-padding-top-remove">
        @foreach( $services as $service)
            @include('site.services.el')
        @endforeach
    </section>
@endsection