@extends('site.layout', ['title' => $service->seo_title, 'ogg_image' => Storage::url($service->middle_preview), 'og_title' => $service->seo_title, 'og_description' => $service->seo_description, 'description' => $service->seo_description, 'keywords' => $service->seo_keywords])
@section('content')
    <section class="uk-block top-block uk-padding-remove">
        <div class="uk-width-1-1 uk-container-center">
            <div class="owl-carousel item-slider">
                <figure>
                    <img src="{{ Storage::url($service->img('large', 'img')) }}">
                </figure>
                @foreach($service->commonImages as $item)
                    <figure>
                        <img src="{{ Storage::url($item->img('default', 'img')) }}">
                    </figure>
                @endforeach
            </div>
        </div>
    </section>
    <section class="uk-block uk-text-center uk-margin-remove uk-padding-top-remove item-description-wrapper">
        <div class="uk-grid uk-width-large-2-3 uk-width-small-3-4 uk-grid-collapse uk-container-center description-item">
            <div class="uk-width-large-3-4 uk-width-medium-1-1 uk-width-small-1-1 uk-text-left text-description">
                <h1 class="uk-text-left title-item">{{ $service->name }}</h1>
                {!! $service->description !!}
            </div>
        </div>
    </section>
    {{--<section class="uk-block uk-margin-large-bottom">--}}
        {{--<div class="uk-grid uk-width-2-3 uk-grid-small uk-container-center recommend-b">--}}
            {{--<h4 class="uk-width-1-1 title-recommend uk-text-uppercase">--}}
                {{--Проекты--}}
            {{--</h4>--}}
            {{--@foreach($projects as $item)--}}
                {{--<a href="{{ route('project.show', ['id' => $item->slug]) }}" class="more-works uk-width-large-1-4 uk-width-medium-1-1 uk-width-small-1-1 uk-link uk-link-reset">--}}
                    {{--<div class="uk-panel">--}}
                        {{--<img src="{{ Storage::url($item->img('middle','img')) }}" alt="">--}}
                        {{--<div>--}}
                            {{--<h4 class="uk-text-center uk-margin-top">--}}
                                {{--{{ $item->name }}--}}
                            {{--</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--@endforeach--}}
        {{--</div>--}}
    {{--</section>--}}
@endsection