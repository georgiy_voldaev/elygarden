<div class="uk-block card-rotate" data-uk-scrollspy="{cls:{{ ($loop->index + 1) % 2 === 0 ? "'uk-animation-slide-right'" : "'uk-animation-slide-left'" }}}">
    <div class="uk-width-2-3 uk-grid uk-grid-width-small-1-1 uk-grid-width-medium-1-1 uk-grid-width-large-1-2 uk-grid-collapse uk-container-center">
        <div class="{{ ($loop->index + 1) % 2 !== 0 ? '' : 'uk-hidden-large' }} img-padding-left">
            <img src="{{ Storage::url($service->img('min', 'img')) }}" alt="" class="uk-responsive-width  {{ ($loop->index + 1) % 2 !== 0 ? '' : 'uk-hidden-large' }}">
        </div>
        <div class="description-services {{ ($loop->index + 1) % 2 !== 0 ? 'description-padding' : '' }}">
            <h3 class="title-cascad-card">
                {{ $service->name }}
            </h3>
            <div class="description-card-rotate">
                {{ $service->short_description }}
            </div>
            <a href="{{ route('service.show', ['id' => $service->slug]) }}" class="btn-inverse uk-button uk-button-primary">Подробнее</a>
        </div>
        <div class="{{ ($loop->index + 1) % 2 === 0 ? '' : 'uk-hidden-large' }} img-padding-right">
            <img src="{{ Storage::url($service->img('min', 'img')) }}" alt="" class="uk-responsive-width uk-visible-large">
        </div>
    </div>
</div>