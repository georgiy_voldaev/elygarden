@extends('site.layout',
    [
        'title' => $commonSeo->contacts_title, 
        'ogg_image' => '', 
        'og_title' => $commonSeo->contacts_title, 
        'og_description' => $commonSeo->contacts_description, 
        'description' => $commonSeo->contacts_description, 
        'keywords' => $commonSeo->contacts_keywords
    ]
)
@section('content')
    <header data-parallax="scroll" data-z-index="0" data-speed="0.5" data-image-src="/img/covers/3.jpg" class="parallax-window uk-margin-large-top uk-overlay top-block common-header">
        <div class="uk-overlay-panel overlay-about uk-vertical-align">
            <div class="uk-vertical-align-middle uk-width-1-1">
                <div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1 uk-container-center">
                    <h1 class="part-site-title">Контакты</h1>
                </div>
            </div>
        </div>
    </header>
    <section class="uk-block uk-text-center uk-padding-remove">
        <div class="uk-block card-rotate">
            <div class="uk-width-2-3 uk-container-center uk-grid">
                <div class="info-contacts-block uk-width-1-1 uk-grid-width-1-3 uk-grid uk-grid-collapse">
                    <div class="uk-grid uk-margin-remove uk-grid-collapse info-contact-item">
                        <div class="uk-float-left contact-ico-wrap">
                            <div class="contact-icon">
                                <i class="uk-icon-phone"></i>
                            </div>
                        </div>
                        <div class="uk-float-2-3 contact-text">
                            <h3 class="uk-text-left uk-text-uppercase uk-text-bold">Телефон</h3>
                            <div class="uk-text-muted uk-text-left">
                            +7 (960) 222-25-25 <br>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid uk-margin-remove uk-grid-collapse info-contact-item">
                        <div class="uk-float-left contact-ico-wrap">
                            <div class="contact-icon">
                                <i class="uk-icon-envelope-o"></i>
                            </div>
                        </div>
                        <div class="uk-float-2-3 contact-text">
                            <h3 class="uk-text-left uk-text-uppercase uk-text-bold">Email</h3>
                            <div class="uk-text-muted uk-text-left">
                                info@elygarden.ru
                            </div>
                        </div>
                    </div>
                    <a href="https://www.instagram.com/ely.garden/" class="uk-grid uk-margin-remove uk-grid-collapse info-contact-item">
                        <div class="uk-float-left contact-ico-wrap">
                            <div class="contact-icon">
                                <i class="uk-icon-instagram"></i>
                            </div>
                        </div>
                        <div class="uk-float-2-3 contact-text">
                            <h3 class="uk-text-left uk-text-uppercase uk-text-bold">Инстаграм</h3>
                            <div class="uk-text-muted uk-text-left">
                                ely.garden
                            </div>
                        </div>
                    </a>
                    <a href="https://tele.click/makarovofficialrus" class="uk-grid uk-margin-remove uk-grid-collapse info-contact-item">
                        <div class="uk-float-left contact-ico-wrap">
                            <div class="contact-icon">
                                <i class="uk-icon-send"></i>
                            </div>
                        </div>
                        <div class="uk-float-2-3 contact-text">
                            <h3 class="uk-text-left uk-text-uppercase uk-text-bold">Telegram</h3>
                            <div class="uk-text-muted uk-text-left">
                                makarovofficialrus
                            </div>
                        </div>
                    </a>
                    <a href="https://api.whatsapp.com/send?phone=796022222525" class="uk-grid uk-margin-remove uk-grid-collapse info-contact-item">
                        <div class="uk-float-left contact-ico-wrap">
                            <div class="contact-icon">
                                <i class="uk-icon-whatsapp"></i>
                            </div>
                        </div>
                        <div class="uk-float-2-3 contact-text">
                            <h3 class="uk-text-left uk-text-uppercase uk-text-bold">Whatsapp</h3>
                            <div class="uk-text-muted uk-text-left">
                                +79602222525
                            </div>
                        </div>
                    </a>
                </div>
{{--                <div class="form-contacts uk-width-large-2-3 uk-width-medium-1-1 uk-width-small-1-1">--}}
{{--                    {{ Form::open(['class'=> 'uk-form uk-grid uk-grid-collapse', 'method' => 'POST', 'url' => route('problem.store')]) }}--}}
{{--                        @if ($errors->any())--}}
{{--                            <div class="uk-width-1-1 uk-text-left uk-alert uk-alert-danger b-validate-feedback" data-uk-alert>--}}
{{--                                <a href="" class="uk-alert-close uk-close"></a>--}}
{{--                                <ul class="uk-list">--}}
{{--                                    @foreach ($errors->all() as $error)--}}
{{--                                        <li>{{ $error }}</li>--}}
{{--                                    @endforeach--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        @if (Session::has('problem_success'))--}}
{{--                            <div class="uk-width-1-1 uk-text-left uk-alert uk-alert-success b-validate-feedback validate-green" data-uk-alert>--}}
{{--                                <a href="" class="uk-alert-close uk-close"></a>--}}
{{--                                <ul>--}}
{{--                                    <li>{{  Session::get('problem_success') }}</li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        <div class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-1-3 wrap-contact-input wrap-contact-input-left">--}}
{{--                            <input type="text" placeholder="Имя" name="name" class="uk-width-1-1">--}}
{{--                            <input type="hidden" name="status" value="1">--}}
{{--                        </div>--}}
{{--                        <div class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-1-3 wrap-contact-input wrap-contact-input-center">--}}
{{--                            <input type="text" placeholder="Email" name="email" class="uk-width-1-1">--}}
{{--                        </div>--}}
{{--                        <div class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-1-3 wrap-contact-input wrap-contact-input-right">--}}
{{--                            <input type="text" placeholder="Номер телефона" name="phone" class="uk-width-1-1">--}}
{{--                        </div>--}}
{{--                        <div class="uk-width-1-1 wrap-contact-input">--}}
{{--                            <textarea placeholder="Сообщение" name="message" id="" cols="30" rows="10" class="uk-width-1-1"></textarea>--}}
{{--                        </div>--}}
{{--                        <p class="uk-width-1-1 uk-text-left">--}}
{{--                            Нажимая кнопку отправить, вы соглашаетесь с <a href="/policy">политикой конфиденциальности.</a>--}}
{{--                            <br>--}}
{{--                            <br>--}}
{{--                        </p>--}}
{{--                        <button class="btn-inverse uk-button uk-button-primary uk-width-large-1-3 uk-width-medium-1-1 uk-width-small-1-1 btn-contact">Отправить</button>--}}
{{--                    {{ Form::close() }}--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
@endsection