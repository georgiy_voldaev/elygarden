@extends('site.layout', 
    [
        'title' => $project->seo_title, 
        'ogg_image' => '', 
        'og_title' => $project->seo_title, 
        'og_description' => $project->seo_description, 
        'description' => $project->seo_description, 
        'keywords' => $project->seo_keywords
    ]
)
@section('content')
    <section class="uk-block top-block uk-padding-remove">
        <div class="uk-width-1-1 uk-container-center">
            <div class="owl-carousel item-slider">
                <figure>
                    <img src="{{ Storage::url($project->img('large', 'img')) }}">
                </figure>
                @foreach($project->commonImages as $item)
                    <figure>
                        <img src="{{ Storage::url($item->img('default', 'img')) }}">
                    </figure>
                @endforeach
            </div>
        </div>
    </section>
    <section class="uk-block uk-text-center uk-margin-remove uk-padding-top-remove item-description-wrapper">
        <div class="uk-grid uk-width-large-2-3 uk-width-small-3-4 uk-grid-collapse uk-container-center description-item">
            <div class="uk-width-large-3-4 uk-width-medium-1-1 uk-width-small-1-1 uk-text-left text-description">
               <h1 class="uk-text-left title-item">{{ $project->name }}</h1>
               {!! $project->description !!}
            </div>
            <div class="uk-width-large-1-4 uk-width-small-1-1 uk-width-medium-1-1">
                <div class="uk-panel uk-text-left info-item-box">
                    <ul class="uk-nav uk-nav-side item-charact">
                        <li class="uk-nav-header uk-padding-remove">
                            <i class="uk-icon-map-marker icon-item"></i>
                            <span>Местоположение</span>
                        </li>
                        <a class="value-charact">{{ $project->location }}</a>
                        <li class="uk-nav-header uk-padding-remove">
                            <i class="uk-icon-tags icon-item"></i>
                            <span>Категории</span>
                        </li>
                        @foreach($project->categories as $service)
                            <a href="{{ route('service.show', $service->slug) }}" class="uk-badge uk-badge-notification item-categoria uk-link uk-link-reset">
                                {{ $service->name }}
                            </a>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="uk-block uk-margin-large-bottom">
        <div class="uk-grid uk-width-2-3 uk-grid-small uk-container-center recommend-b">
            <h4 class="uk-width-1-1 title-recommend uk-text-uppercase">
                Ещё проекты
            </h4>
            @foreach($recommendedProjects as $item)
                <a href="{{ route('project.show', ['id' => $item->slug]) }}" class="more-works uk-width-large-1-4 uk-width-small-1-1 uk-width-medium-1-1 uk-link uk-link-reset">
                    <div class="uk-panel">
                        <img src="{{ Storage::url($item->img('min', 'img')) }}" alt="">
                        <div>
                            <h4 class="uk-text-center uk-margin-top">
                                {{ $item->name }}
                            </h4>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </section>
@endsection