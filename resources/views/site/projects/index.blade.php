@extends('site.layout', 
    [
        'title' => $commonSeo->projects_title, 
        'ogg_image' => '', 
        'og_title' => $commonSeo->projects_title, 
        'og_description' => $commonSeo->projects_description, 
        'description' => $commonSeo->projects_description, 
        'keywords' => $commonSeo->projects_keywords
    ]
)
@section('content')
    <header data-parallax="scroll" data-z-index="0" data-speed="0.5" data-image-src="/img/covers/15.jpg" class="parallax-window uk-margin-large-top uk-overlay top-block common-header">
        <div class="uk-overlay-panel overlay-about uk-vertical-align">
            <div class="uk-vertical-align-middle uk-width-1-1">
                <div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1 uk-container-center">
                    <h1 class="part-site-title works-index-padding">Наши работы</h1>
                </div>
            </div>
        </div>
    </header>
    <section class="uk-block uk-text-center uk-padding-top-remove uk-margin-top-remove">
        <div class="work-place uk-grid uk-grid-width-medium-1-1 uk-grid-width-small-1-1 uk-grid-width-large-1-1 uk-width-1-1 uk-grid-collapse uk-container-center">
            @foreach($projects as $project)
                @include('site.projects.el')
            @endforeach
        </div>
        <div class="uk-width-1-1 uk-text-center">
            <div class="cssload-jumping">
                <span></span><span></span><span></span><span></span><span></span>
            </div>
        </div>
    </section>
@endsection