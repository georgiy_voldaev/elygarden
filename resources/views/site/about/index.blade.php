@extends('site.layout', 
    [
        'title' => $commonSeo->about_title, 
        'og_title' => $commonSeo->about_title, 
        'ogg_image' => '', 
        'og_description' => $commonSeo->about_description, 
        'description' => $commonSeo->about_description, 
        'keywords' => $commonSeo->about_keywords
    ]
)
@section('content')
    <header data-parallax="scroll" data-z-index="0" data-speed="0.5" data-image-src="/img/covers/4.jpg" class="parallax-window uk-margin-large-top uk-overlay top-block common-header">
        <div class="uk-overlay-panel overlay-about uk-vertical-align">
            <div class="uk-vertical-align-middle uk-width-1-1">
                <div class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1 uk-container-center">
                    <h1 class="part-site-title">О нас</h1>
                </div>
            </div>
        </div>
    </header>
    <section class="uk-block uk-text-center about-white-section">
        <div class="uk-width-2-3 uk-grid uk-grid-width-large-1-2 uk-grid-width-small-1-1 uk-grid-width-medium-1-1 uk-container-center uk-margin-large">
            <div class="item-about-main">
                <h3 class="about-title-ico">Миссия</h3>
                <div class="description uk-text-justify">
                    Элизиум - синоним слова "рай". 
                    Кому не захочется провести свою жизнь в райском саду? 
                    Наша миссия - создание тепла и комфорта для наших клиентов, 
                    торжество природной красоты над серым миром стекла и бетона. 
                    Каждый проект для нас – собственный Райский сад. 
                    Сочетание современных решений, традиций ландшафтной архитектуры и высокого 
                    профессионализма позволяют создавать поистине потрясающие сады.
                </div>
            </div>
            <div class="item-about-main">
                <h3 class="about-title-ico about-title-ico-second">Возможности</h3>
                <div class="description uk-text-justify">
                    На сегодняшний день садово-парковая компания Элизиум
                    предлагает полный перечень работ по благоустройству прилегающих территорий, 
                    включающее в себя инженерное проектирование, строительство и последующее обслуживание.
                </div>
            </div>
        </div>
    </section>
    
    @include('site.about.elements.workers')
    @include('site.about.elements.licens')
@endsection
