<section class="uk-block uk-text-center">
    <h2 class="title-screen title-about-screen">Лицензии</h2>
    <div class="uk-width-2-3 uk-grid uk-grid-collapse uk-grid-width-large-1-3 uk-grid-width-medium-1-1 uk-grid-width-small-1-1 uk-container-center uk-padding-remove">
        <div class="uk-block wrap-licen uk-container-center">
            <a class="licen-card uk-link uk-link-reset" href="img/covers/sertificate.jpg" data-lightbox-type="image" data-uk-lightbox="{group:'group'}" title="-">
                <img src="img/covers/sertificate.jpg" alt="" class="uk-responsive-width">
                <div class="uk-text-center">
                    <h3 class="uk-text-uppercase uk-text-center uk-text-bold uk-margin-small-top">
                        
                    </h3>
                    <div class="uk-text-muted">
                        
                    </div>
                </div>
            </a>
        </div>
    </div>
</section>