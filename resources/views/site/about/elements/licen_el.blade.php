<div class="uk-block wrap-licen">
    <a class="licen-card uk-link uk-link-reset" href="{{ Storage::url($licen->img_large) }}" data-lightbox-type="image" data-uk-lightbox="{group:'group'}" title="{{ $licen->name }}">
        <img src="{{ Storage::url($licen->img) }}" alt="" class="uk-responsive-width">
        <div class="uk-text-center">
            <h3 class="uk-text-uppercase uk-text-center uk-text-bold uk-margin-small-top">
                
            </h3>
            <div class="uk-text-muted">
                
            </div>
        </div>
    </a>
</div>
