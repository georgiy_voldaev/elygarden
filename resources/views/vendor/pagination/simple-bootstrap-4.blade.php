@if ($paginator->hasPages())
    <ul class="uk-pagination list-pagination uk-width-1-1" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="uk-pagination-previous disabled" aria-disabled="true">
                <span class="page-link"><i class="uk-icon-angle-left icon-pagination"></i> Назад</span>
            </li>
        @else
            <li class="uk-pagination-previous">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="uk-icon-angle-left icon-pagination"></i> Назад</a>
            </li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="uk-pagination-next">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">Вперед <i class="uk-icon-angle-right icon-pagination"></i></a>
            </li>
        @else
            <li class="uk-pagination-next disabled" aria-disabled="true">
                <span class="page-link">Вперед <i class="uk-icon-angle-right icon-pagination"></i></span>
            </li>
        @endif
    </ul>
@endif
