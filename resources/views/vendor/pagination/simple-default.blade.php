@if ($paginator->hasPages())
    <ul class="uk-pagination list-pagination uk-width-1-1 pagination-catalog" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="uk-pagination-previous disabled" aria-disabled="true"><span>@lang('pagination.previous')</span></li>
        @else
            <li class="uk-pagination-previous"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="uk-pagination-next"><a href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a></li>
        @else
            <li class="uk-pagination-next disabled" aria-disabled="true"><span>@lang('pagination.next')</span></li>
        @endif
    </ul>
@endif
